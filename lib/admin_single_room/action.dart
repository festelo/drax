import 'package:fish_redux/fish_redux.dart';
import '../shared_states.dart';

enum AdminSingleRoomAction{
  changeInfo,
  changePhoto,
  refreshSilent, 
  refresh,
  setLoading,
  saveId
}

class AdminSingleRoomActionCreate{
  static Action changeInfo() => const Action(AdminSingleRoomAction.changeInfo);
  static Action changePhoto() => const Action(AdminSingleRoomAction.changePhoto);
  static Action setLoading(bool loading) => Action(AdminSingleRoomAction.setLoading, payload: loading);
  static Action refreshSilent({RoomState room, bool enabled, bool favorite}) => 
    Action(AdminSingleRoomAction.refreshSilent, 
      payload: {
        'room': room,
        'enabled': enabled,
        'favorite': favorite
      });
  static Action refresh({String newId}) => Action(AdminSingleRoomAction.refresh, payload: {'id': newId});
  static Action saveId() => const Action(AdminSingleRoomAction.saveId);
}