import 'package:fish_redux/fish_redux.dart';
import 'package:fish_redux/fish_redux.dart' as fish;
import 'package:flutter/widgets.dart';

import 'action.dart';
import 'state.dart';
import '../shared_states.dart';

Reducer<AdminSingleRoomState> buildReducer() {
  return asReducer({
    AdminSingleRoomAction.refreshSilent: _refreshSilent,
    AdminSingleRoomAction.setLoading: _setLoading,
  });
}

AdminSingleRoomState _setLoading(AdminSingleRoomState state, fish.Action action) {
  return state.clone()
    ..loading = action.payload;
}

AdminSingleRoomState _refreshSilent(AdminSingleRoomState state, fish.Action action) {
  RoomState room = action.payload['room'] ?? state.room;
  final enabled = action.payload['enabled'] ?? room?.enabled;
  final favorite = action.payload['favorite'] ?? room?.favorite;
  room?.enabled = enabled;
  room?.favorite = favorite;
  return state.clone()
    ..room = room
    ..nameController = state.room?.name == room?.name 
      ? state.nameController
      : TextEditingController(text: room?.name);
}