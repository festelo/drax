import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import '../shared_states.dart';
import 'page.dart';
import '../mixins.dart';

class AdminSingleRoomState with DatabaseState, StorageState implements Cloneable<AdminSingleRoomState> {
  GlobalKey<State<Scaffold>> scaffoldKey;
  TextEditingController nameController;
  RoomState room;
  String userId;
  bool loading = false;

  @override
  AdminSingleRoomState clone() {
    return AdminSingleRoomState()
      ..scaffoldKey = scaffoldKey
      ..nameController = nameController
      ..userId = userId
      ..room = room
      ..loading = loading;
  }
}

AdminSingleRoomState initState(AdminSingleRoomPageParams params) {
  final state = AdminSingleRoomState()
    ..scaffoldKey = GlobalKey()
    ..nameController = TextEditingController()
    ..userId = params.userId
    ..room = (RoomState()..id = params.roomId);
  return state;
}