import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

import '../components.dart';
import 'state.dart';
import 'action.dart';

Widget buildView(AdminSingleRoomState state, Dispatch dispatch, ViewService viewService) {
  return Scaffold(
    key: state.scaffoldKey,
    body: Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          fit: BoxFit.cover,
          alignment: Alignment.topLeft,
          colorFilter: new ColorFilter.mode(Color(0xFF373447), BlendMode.dstATop),
          image: AssetImage('assets/backs/login.png')
        )
      ),
      padding: const EdgeInsets.symmetric(horizontal: 0.0),
      child: CustomScrollView( 
        slivers: [
          SliverAppBar(
            centerTitle: true,
            backgroundColor: Colors.transparent,
            title: Text('Admin panel', style: TextStyle(fontSize: 35, fontWeight: FontWeight.w500),),
          ),
          SliverPadding(
            padding: EdgeInsets.symmetric(horizontal: 16),
            sliver: SliverList(
              delegate: SliverChildListDelegate(
                [
                  Padding(
                    padding: EdgeInsets.only(top: 50),
                    child: Text('Room info', style: TextStyle(fontSize: 30, fontWeight: FontWeight.w500), textAlign: TextAlign.center,),
                  ),
                  FlatButton(
                    child: Text(state.room.id ?? 'Not saved room'),
                    onPressed: state.room.id == null 
                      ? null 
                      : () => dispatch(AdminSingleRoomActionCreate.saveId()),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 20),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                      border: Border.all(
                        color: Colors.grey,
                        width: 2
                      )
                    ),
                    child: Column(
                      children: [
                        DraxTextField(
                          leftIcon: Icon(Icons.tag_faces), 
                          controller: state.nameController,
                          hintText: 'Room name',
                        ),/*
                        SwitchListTile(
                          title: Text('Enabled', style: TextStyle(color: Colors.white),),
                          activeColor: Colors.white,
                          onChanged: (val) => dispatch(AdminSingleRoomActionCreate.refreshSilent(enabled: !state.room.enabled)),
                          value: state.room.enabled,
                        ),*/
                        SwitchListTile(
                          title: Text('Favorite', style: TextStyle(color: Colors.white),),
                          activeColor: Colors.white,
                          onChanged: (val) => dispatch(AdminSingleRoomActionCreate.refreshSilent(favorite: !state.room.favorite)),
                          value: state.room.favorite,
                        ),
                        RectButton(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.vertical(bottom: Radius.circular(10)),
                            border: Border.all()
                          ),
                          text: 'Save',
                          margin: EdgeInsets.only(top: 1),
                          onPressed: state.loading ? null : () => dispatch(AdminSingleRoomActionCreate.changeInfo()),
                        )
                      ],
                    ),
                  ),
                  RectButton(
                    decoration: BoxDecoration(
                      border: Border.all(color: Colors.grey, width: 2)
                    ),
                    margin: EdgeInsets.only(top: 40),
                    text: 'Change photo',
                    onPressed: state.loading ? null : () => dispatch(AdminSingleRoomActionCreate.changePhoto()),
                  )
                ]
              ),
            ) 
          )
        ]
      )
    )
  );
}