import 'package:fish_redux/fish_redux.dart';

import 'effect.dart';
import 'state.dart';
import 'view.dart';
import 'reducer.dart';

class AdminSingleRoomPageParams {
  final String userId;
  final String roomId;
  AdminSingleRoomPageParams.createNew(this.userId) : roomId = null;
  AdminSingleRoomPageParams.change(this.roomId) : userId = null;
}

class AdminSingleRoomPage extends Page<AdminSingleRoomState, AdminSingleRoomPageParams> {
  AdminSingleRoomPage()
      : super(
          initState: initState,
          effect: buildEffect(),
          view: buildView,
          reducer: buildReducer(),
          middleware: [
            logMiddleware(tag: 'AdminSingleRoomPage'),
          ],
        );
}