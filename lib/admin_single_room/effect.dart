import 'package:fish_redux/fish_redux.dart';
import 'package:fish_redux/fish_redux.dart' as fish;
import 'package:flutter/material.dart';

import '../shared_effects.dart';
import '../shared_states.dart';
import '../router.dart';
import 'state.dart';
import 'action.dart';

Effect<AdminSingleRoomState> buildEffect(){
  return combineEffects({
    AdminSingleRoomAction.saveId: (a, c) => saveToClipboard(c.state.room.id),
    AdminSingleRoomAction.changePhoto: _changePhoto,
    AdminSingleRoomAction.changeInfo: _changeInfo,
    AdminSingleRoomAction.refresh: _refresh,
    Lifecycle.initState: _initState
  });
}

void _initState(fish.Action action, Context<AdminSingleRoomState> context) {
  context.dispatch(AdminSingleRoomActionCreate.refresh());
}

Future<void> _refresh(fish.Action action, Context<AdminSingleRoomState> context) async {
  final id = action.payload['id'] ?? context.state.room?.id;
  if(id != null) {
    final room = await context.state.database.rooms.get(id);
    context.dispatch(AdminSingleRoomActionCreate.refreshSilent(room: room));
  }
}

Future<void> _changeInfo(fish.Action action, Context<AdminSingleRoomState> context) async {
  final oldRoom = context.state.room;
  var id = oldRoom.id;
  final db = context.state.database;
  context.dispatch(AdminSingleRoomActionCreate.setLoading(true));
  if(id != null) {
    await db.rooms.update(oldRoom.id, oldRoom.toMap()..addAll({
      'name': context.state.nameController.text,
    }));
  }
  else {
    final userId = context.state.userId;
    id = await db.rooms.create(oldRoom, additional: {
      'name' : context.state.nameController.text,
      'owner': context.state.database.store.collection('users').document(userId)
    });
    await db.users.addRoom(userId, db.rooms.document(id));
  }
  context.dispatch(AdminSingleRoomActionCreate.refresh(newId: id));
  context.dispatch(AdminSingleRoomActionCreate.setLoading(false));
}

Future<void> _changePhoto(fish.Action action, Context<AdminSingleRoomState> context) async {
  final db = context.state.database;
  final storage = context.state.storage;
  final room = context.state.room;
  if(room.id == null) {
    showSnackMessage(context.state.scaffoldKey.currentState, 'You should to set info first');
    return;
  }
  else {
    final file = await showChangePhotoDialog(context.context);
    if(file == null) return;
    context.dispatch(AdminSingleRoomActionCreate.setLoading(true));
    final url = await storage.upload(file, 'room_${room.id}');
    await db.rooms.update(room.id, {
      'imageUrl': url,
      'localImage': false
    });
    context.dispatch(AdminSingleRoomActionCreate.refresh());
    context.dispatch(AdminSingleRoomActionCreate.setLoading(false));
  }
}