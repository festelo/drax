import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

import '../components.dart';
import 'state.dart';
import 'action.dart';

Widget buildView(RegisterState state, Dispatch dispatch, ViewService viewService) {
  return Scaffold(
    key: state.scaffoldKey,
    body: Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          fit: BoxFit.cover,
          alignment: Alignment.topLeft,
          colorFilter: new ColorFilter.mode(Color(0xFF373447), BlendMode.dstATop),
          image: AssetImage('assets/backs/login.png')
        )
      ),
      padding: const EdgeInsets.only(top: 20),
      child: CustomScrollView( 
        slivers: [
          SliverAppBar(
            centerTitle: true,
            backgroundColor: Colors.transparent,
            title: Text('Admin panel', style: TextStyle(fontSize: 35, fontWeight: FontWeight.w500),),
          ),
          SliverPadding(
            padding: EdgeInsets.symmetric(horizontal: 16),
            sliver: SliverList(
              delegate: SliverChildListDelegate(
                [
                  Padding(
                    padding: EdgeInsets.only(top: 60),
                    child: Text('Registration', style: TextStyle(fontSize: 30, fontWeight: FontWeight.w500), textAlign: TextAlign.center,),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 30),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                      border: Border.all(
                        color: Colors.grey,
                        width: 2
                      )
                    ),
                    child: Column(
                      children: [
                        DraxTextField(
                          leftIcon: Icon(Icons.tag_faces), 
                          controller: state.nameController,
                          hintText: 'First & Last name',
                          scrollPadding: EdgeInsets.all(80),
                        ),
                        DraxTextField(
                          leftIcon: Icon(Icons.mail_outline), 
                          controller: state.emailEditController,
                          hintText: 'example@domain.com',
                          scrollPadding: EdgeInsets.all(120),
                        ),
                        DraxTextField(
                          leftIcon: Icon(Icons.lock_outline), 
                          controller: state.passwordEditController,
                          obscureText: true,
                          hintText: 'Password',
                          padding: EdgeInsets.only(top: 10),
                          scrollPadding: EdgeInsets.all(80),
                        ),
                        RectButton(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.vertical(bottom: Radius.circular(10)),
                            border: Border.all()
                          ),
                          text: 'Create',
                          margin: EdgeInsets.only(top: 1),
                          onPressed: state.loading ? null : () => dispatch(AdminRegisterActionCreate.register()),
                        )
                      ],
                    ),
                  ),
                ]
              ),
            ) 
          )
        ]
      )
    )
  );
}