import 'package:fish_redux/fish_redux.dart';

enum AdminRegisterAction{
  createAccount,
  register,
  login,
  goBack,
  changePhoto,
  openUserChange,
  setLoading
}

class AdminRegisterActionCreate{
  static Action openUserChange(String id) => Action(AdminRegisterAction.openUserChange, payload: id);
  static Action createAccount(String id) => Action(AdminRegisterAction.createAccount, payload: {'id' : id});
  static Action register() => const Action(AdminRegisterAction.register);
  static Action login() => const Action(AdminRegisterAction.login);
  static Action goBack() => const Action(AdminRegisterAction.goBack);
  static Action changePhoto() => const Action(AdminRegisterAction.changePhoto);
  static Action setLoading(bool value) =>  Action(AdminRegisterAction.setLoading, payload: value);
} 