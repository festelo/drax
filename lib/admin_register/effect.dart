import 'package:cloud_functions/cloud_functions.dart';
import 'package:fish_redux/fish_redux.dart';
import 'package:fish_redux/fish_redux.dart' as fish;
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'state.dart';
import 'action.dart';
import '../router.dart';
import '../shared_states.dart';
import '../shared_effects.dart';


Effect<RegisterState> buildEffect(){
  return combineEffects({
      AdminRegisterAction.register: _register,
      AdminRegisterAction.createAccount: _createAccount,
      AdminRegisterAction.openUserChange: _openUserChange,
      AdminRegisterAction.goBack: (a , b) => goBack(b.context),
  });
}

Future<void> _createAccount(fish.Action action, Context<RegisterState> context) async {
  final id = action.payload['id'];
  final name = context.state.nameController.text;
  try {
    final user = UserState()
      ..id = id
      ..name = name;
    final exist = await context.state.database.users.exist(user.id);
    if(exist) throw Exception('User already exist');
    await context.state.database.users.createWithId(user, user.id);
    print('account created');
  }
  catch (ex) {
    showSnackMessage(context.state.scaffoldKey.currentState, ex.toString());
    print('account creation failed: ' + ex.toString());
  }
  finally {
    context.dispatch(AdminRegisterActionCreate.openUserChange(id));
  }
}

Future<void> _register(fish.Action action, Context<RegisterState> context) async {
  final email = context.state.emailEditController.text;
  final pass = context.state.passwordEditController.text;
  context.dispatch(AdminRegisterActionCreate.setLoading(true));
  try {
    final res = await context.state.functions.call('createAccount', {
      'email': email,
      'password': pass
    });
    context.dispatch(AdminRegisterActionCreate.createAccount(res['user']['uid']));
  }
  on CloudFunctionsException {
    showSnackMessage(context.state.scaffoldKey.currentState, 'Check the email and password. Password must be at least 6 symbols.');
  }
  catch (ex) {
    showSnackMessage(context.state.scaffoldKey.currentState, ex.toString());
    print('registration failed: ' + ex.toString());
  }
  finally {
    context.dispatch(AdminRegisterActionCreate.setLoading(false));
  }
}

void _openUserChange(fish.Action action, Context<RegisterState> context){
  final route = Routes.adminChangeUser(userId: action.payload);
  Navigator
    .of(context.context)
    .pushReplacementNamed(route);
}