import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import '../mixins.dart';

class RegisterState with FunctionsState, DatabaseState implements Cloneable<RegisterState> {
  GlobalKey<State<Scaffold>> scaffoldKey;
  TextEditingController nameController;
  TextEditingController emailEditController;
  TextEditingController passwordEditController;
  bool loading = false;

  @override
  RegisterState clone() {
    return RegisterState()
      .. scaffoldKey            = scaffoldKey
      .. nameController         = nameController
      .. emailEditController    = emailEditController
      .. passwordEditController = passwordEditController
      .. loading                = loading;
  }
}

RegisterState initState(dynamic param) {
  final RegisterState state = RegisterState();
  state.nameController = TextEditingController();
  state.emailEditController = TextEditingController();
  state.passwordEditController = TextEditingController();
  state.scaffoldKey = GlobalKey();
  return state;
}