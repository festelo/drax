import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

import '../components.dart';
import 'state.dart';
import 'action.dart';

Widget buildView(LoginState state, Dispatch dispatch, ViewService viewService) {
  return Scaffold(
    key: state.scaffoldKey,
    body: Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          fit: BoxFit.cover,
          alignment: Alignment.topLeft,
          colorFilter: new ColorFilter.mode(Color(0xFF373447), BlendMode.dstATop),
          image: AssetImage('assets/backs/login.png')
        )
      ),
      padding: const EdgeInsets.all(16.0),
      child: Column(
        children: [ 
          Expanded(
            child: Center(
              child: ConstrainedBox(
                constraints: BoxConstraints(
                  maxHeight: 475,
                  maxWidth: 375
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Image(
                          image: AssetImage('assets/draxo.png')
                        ),
                        !state.adminLogin 
                          ? Container() 
                          : Padding(
                            padding: EdgeInsets.only(bottom: 16),
                            child: Text('Admin', style: TextStyle(fontSize: 16, fontWeight: FontWeight.w600),),
                          ),
                      ],
                    ),
                    ConstrainedBox(
                      constraints: BoxConstraints(
                        maxHeight: 220
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            children: [
                              DraxTextField(
                                leftIcon: Icon(Icons.mail_outline), 
                                controller: state.emailEditController,
                                hintText: 'example@domain.com',
                              ),
                              DraxTextField(
                                leftIcon: Icon(Icons.lock_outline), 
                                controller: state.passwordEditController,
                                obscureText: true,
                                hintText: 'Password',
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  FlatButton(
                                    child: Text(
                                      'Forgot your password?', 
                                      style: TextStyle(fontSize: 12, color: Color(0xD5DBDBDB)),
                                    ), 
                                    onPressed: () {},
                                  ),
                                  FlatButton(
                                    child: Text(
                                      'Auto-Login', 
                                      style: TextStyle(fontSize: 12, color: Color(0xD5DBDBDB)),
                                    ), 
                                    onPressed: () {},
                                  ),
                                ],
                              )
                            ],
                          ),
                          RectButton(
                            width: 237, 
                            text: 'Log In',
                            onPressed: () => dispatch(LoginActionCreate.login()),
                          )
                        ],
                      ),
                    )
                  ]
                )
              )
            ),
          ),
          ConstrainedBox(
            constraints: BoxConstraints(maxWidth: 375),
            child: Row(
                children: [ 
                  Container(width: 55,),
                  Expanded(
                    child:  state.adminLogin 
                      ? Container() 
                      : RichText(
                        textAlign: TextAlign.center,
                        text: TextSpan(
                          children: [
                            TextSpan(text: "Don't have an account? "),
                            TextSpan(
                              text: 'Create one', 
                              style: TextStyle(height: 1, color: Color(0xFF79729F)), 
                              recognizer: TapGestureRecognizer()
                                ..onTap =  () => dispatch(LoginActionCreate.goToRegisterPage())
                            )
                          ],
                          style: TextStyle(color: Color(0xFF7B7984), fontSize: 11)
                        ),
                      ),
                  ),
                  Container(
                    width: 55,
                    child: FlatButton(
                      padding: EdgeInsets.all(0),
                      child: Text(
                        state.adminLogin ? 'User' : 'Admin',
                        style: TextStyle(fontSize: 11, color: Color(0xD5DBDBDB)),
                      ),
                      onPressed: () => dispatch(LoginActionCreate.switchAdmin()),
                    ),
                  )
                ]
              )
          )
        ]
      )
    )
  );
}
