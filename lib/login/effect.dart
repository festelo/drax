import 'package:fish_redux/fish_redux.dart';
import 'package:fish_redux/fish_redux.dart' as fish;
import 'package:flutter/material.dart';

import '../router.dart';
import 'state.dart';
import 'action.dart';
import '../shared_effects.dart';
import '../shared_states.dart';

Effect<LoginState> buildEffect(){
  return combineEffects({
    LoginAction.login: _login,
    LoginAction.goToRegisterPage: _goToRegisterPage,
    LoginAction.forgotPassword: (a , b) => {},
  });
}

void _goToRegisterPage(fish.Action action, Context<LoginState> context){
  final route = Routes.register(email: context.state.emailEditController.text);
  Navigator
    .of(context.context)
    .pushNamed(route);
}

Future<void> _login(fish.Action action, Context<LoginState> context) async {
  final email = context.state.emailEditController.text;
  final pass = context.state.passwordEditController.text;
  context.dispatch(LoginActionCreate.setLoading(true));
  try {
    final user = await context.state.auth.signIn(email, pass);
    final exist = await context.state.database.users.exist(user.uid);
    if(!exist) {
      showSnackMessage(context.state.scaffoldKey.currentState, "You should to repeat registration");
      throw Exception("User doesn't complete a registration");
    }

    if(context.state.adminLogin) {
      final isAdmin = await context.state.database.users.isAdmin(user.uid);
      if(!isAdmin) {
        showSnackMessage(context.state.scaffoldKey.currentState, "User isn't an admin");
        throw Exception("User isn't an admin");
      }
      logInAdminSilent(Navigator.of(context.context));
    } 
    else {
      logInSilent(context.state.auth, Navigator.of(context.context));
    }
  }
  catch (ex) {
    showSnackMessage(context.state.scaffoldKey.currentState, "Login failed. Check your login & password");
    print('login failed: ' + ex.toString());
  }
  finally {
    context.dispatch(LoginActionCreate.setLoading(false));
  }
}