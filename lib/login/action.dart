import 'package:fish_redux/fish_redux.dart';

enum LoginAction{
  login,
  setLoading,
  goToRegisterPage,
  forgotPassword,
  switchAdmin
}

class LoginActionCreate{
  static Action login() => const Action(LoginAction.login);
  static Action goToRegisterPage() => const Action(LoginAction.goToRegisterPage);
  static Action forgotPassword() => const Action(LoginAction.forgotPassword);
  static Action switchAdmin() => const Action(LoginAction.switchAdmin);
  static Action setLoading(bool value) =>  Action(LoginAction.setLoading, payload: value);
}