import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'page.dart';
import '../mixins.dart';

class LoginState with AuthState, DatabaseState implements Cloneable<LoginState> {
  GlobalKey<State<Scaffold>> scaffoldKey;
  TextEditingController emailEditController;
  TextEditingController passwordEditController;
  bool adminLogin;
  bool loading = false;

  @override
  LoginState clone() {
    return LoginState()
      .. scaffoldKey            = scaffoldKey
      .. emailEditController    = emailEditController
      .. passwordEditController = passwordEditController
      .. adminLogin             = adminLogin
      .. loading                = loading;
  }
}

LoginState initState(LoginPageParams params) {
  final LoginState state = LoginState();
  state.emailEditController = TextEditingController(text: params?.email ?? '');
  state.passwordEditController = TextEditingController();
  state.adminLogin = params?.adminLogin ?? false;
  state.scaffoldKey = GlobalKey();
  return state;
}