import 'package:fish_redux/fish_redux.dart';

import 'effect.dart';
import 'state.dart';
import 'view.dart';
import 'reducer.dart';

class LoginPageParams {
  final String email;
  final bool adminLogin;

  LoginPageParams(this.email, this.adminLogin);
}

class LoginPage extends Page<LoginState, LoginPageParams> {
  LoginPage()
      : super(
          initState: initState,
          effect: buildEffect(),
          view: buildView,
          reducer: buildReducer(),
          middleware: [
            logMiddleware(tag: 'LoginPage'),
          ],
        );
}