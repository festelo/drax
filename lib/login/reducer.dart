import 'package:fish_redux/fish_redux.dart';

import 'action.dart';
import 'state.dart';

Reducer<LoginState> buildReducer() {
  return asReducer<LoginState>(<Object, Reducer<LoginState>>{
    LoginAction.switchAdmin: _switchAdmin,
    LoginAction.setLoading: _setLoading
  });
}

LoginState _setLoading(LoginState state, Action action) {
  return state.clone()
    ..loading = action.payload;
}

LoginState _switchAdmin(LoginState state, Action action) {
  return state.clone()
    ..adminLogin = !state.adminLogin;
}