import 'package:fluro/fluro.dart';
import 'package:flutter/material.dart';
import 'pages.dart';

var loginHandler = new Handler(
    handlerFunc: (context, params) =>  LoginPage().buildPage(
      LoginPageParams(params["email"]?.first, params["adminLogin"]?.first == 'true'))
);

var loadingHandler = new Handler(
    handlerFunc: (context, params) =>  Center(child: CircularProgressIndicator())
);

var registerHandler = new Handler(
    handlerFunc: (context, params) =>  RegisterPage().buildPage(params["email"]?.first)
);

var homeHandler = new Handler(
    handlerFunc: (context, params) =>  HomePage().buildPage(params["userId"]?.first)
);

var roomHandler = new Handler(
    handlerFunc: (context, params) =>  RoomPage().buildPage(params["roomId"]?.first)
);

var userHandler = new Handler(
    handlerFunc: (context, params) =>  UserPage().buildPage(params["userId"]?.first)
);

var adminHandler = new Handler(
    handlerFunc: (context, params) =>  AdminPage().buildPage(null)
);

var adminRoomsHandler = new Handler(
    handlerFunc: (context, params) =>  AdminRoomsPage().buildPage(params['userId']?.first)
);


var adminCreateUserHandler = new Handler(
    handlerFunc: (context, params) =>  AdminRegisterPage().buildPage(null)
);

var adminChangeUserHandler = new Handler(
    handlerFunc: (context, params) =>  AdminUserPage().buildPage(params['userId']?.first)
);

var adminChangeRoomHandler = new Handler(
    handlerFunc: (context, params) =>  AdminSingleRoomPage().buildPage(
      AdminSingleRoomPageParams.change(params['roomId']?.first)
    )
);

var adminCreateRoomHandler = new Handler(
    handlerFunc: (context, params) =>  AdminSingleRoomPage().buildPage(
      AdminSingleRoomPageParams.createNew(params['userId']?.first)
    )
);

var adminChangeDeviceHandler = new Handler(
    handlerFunc: (context, params) =>  AdminSingleDevicePage().buildPage(
      AdminSingleDevicePageParams.change(params['deviceId']?.first)
    )
);

var adminCreateDeviceHandler = new Handler(
    handlerFunc: (context, params) =>  AdminSingleDevicePage().buildPage(
      AdminSingleDevicePageParams.createNew(params['roomId']?.first)
    )
);

var adminDevicesHandler = new Handler(
    handlerFunc: (context, params) =>  AdminDevicesPage().buildPage(params['roomId']?.first)
);

var adminActionsHandler = new Handler(
    handlerFunc: (context, params) =>  AdminActionsPage().buildPage(params['deviceId']?.first)
);