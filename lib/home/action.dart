import '../shared_states.dart';
import 'package:fish_redux/fish_redux.dart';

enum HomeAction{
  switchPage,
  changePhoto,
  refreshSilent,
  refresh,
  setLoading,
  openUserPage,
  logout
}

class HomeActionCreate{
  static Action switchPage(int page) => Action(HomeAction.switchPage, payload: page);
  static Action setLoading(bool loading) => Action(HomeAction.setLoading, payload: loading);
  static Action refresh() => Action(HomeAction.refresh);
  static Action refreshSilent(UserState user, List<RoomState> rooms) => 
    Action(
      HomeAction.refreshSilent, 
      payload: {
        'user' : user,
        'rooms' : rooms
      }
    );
  static Action logout() => Action(HomeAction.logout);
  static Action changePhoto() => Action(HomeAction.changePhoto);
  static Action openUserPage() => Action(HomeAction.openUserPage);
} 