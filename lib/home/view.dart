import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'state.dart';
import 'action.dart';

Widget buildView(HomeState state, Dispatch dispatch, ViewService viewService) {
  children() => [
    viewService.buildComponent('favorites_tab'),
    viewService.buildComponent('all_rooms_tab'),
    viewService.buildComponent('all_devices_tab'),
  ];
  return Scaffold(
    body: state.loading ? Center(child: CircularProgressIndicator()) : children()[state.currentPage],
    bottomNavigationBar: Theme(
      data: Theme
        .of(viewService.context)
        .copyWith(
          canvasColor: Colors.black
        ),
      child: BottomNavigationBar(
        type: BottomNavigationBarType.shifting,
        onTap: (i) => dispatch(HomeActionCreate.switchPage(i)),
        currentIndex: state.currentPage, 
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.favorite_border),
            title: Text('Favorites'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            title: Text('Rooms'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.devices_other),
            title: Text('Devices')
          )
        ],
      ),
    )
  );
}