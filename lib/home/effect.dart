import 'package:fish_redux/fish_redux.dart';
import 'package:fish_redux/fish_redux.dart' as fish;
import 'package:flutter/material.dart';

import '../router.dart';
import 'state.dart';
import 'action.dart';
import '../shared_effects.dart';

Effect<HomeState> buildEffect(){
  return combineEffects({
    HomeAction.logout: _logout,
    HomeAction.changePhoto: _changePhoto,
    HomeAction.refresh: _refresh,
    HomeAction.openUserPage: _openUserPage,
    Lifecycle.initState: _initState
  });
}

void _openUserPage(fish.Action action, Context<HomeState> context){
  final route = Routes.user(userId: context.state.user.id);
  Navigator
    .of(context.context)
    .pushNamed(route);
}

Future<void> _initState(fish.Action action, Context<HomeState> context) async {
  context.dispatch(HomeActionCreate.refresh());
}

Future<void> _refresh(fish.Action action, Context<HomeState> context) async {
  final state = context.state;
  if(state.user.id != null) {
    context.dispatch(HomeActionCreate.setLoading(true));
    final userDoc = await state.database.users.document(state.user.id).get();
    final user = state.database.users.fromDocument(userDoc);
    final rooms = await state.database.rooms.getByUserDocument(userDoc);
    context.dispatch(HomeActionCreate.refreshSilent(user, rooms));
    context.dispatch(HomeActionCreate.setLoading(false));
  }
}


void _logout(fish.Action action, Context<HomeState> context){
  logout(context.state.auth);
}
void _changePhoto(fish.Action action, Context<HomeState> context){
  showChangePhotoDialog(
    context.context, 
  );
}