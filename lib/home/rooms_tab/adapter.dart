import 'package:fish_redux/fish_redux.dart';

import 'listed_room/component.dart';
import '../../shared_states.dart';
import 'state.dart';

class RoomListAdapter<T extends RoomsTabState> extends DynamicFlowAdapter<T> {
  RoomListAdapter()
      : super(
          pool: <String, Component<Object>>{
            'room': RoomComponent(),
          },
          connector: _RoomListConnector<T>(),
        );
}

class _RoomListConnector<T extends RoomsTabState> implements ConnOp<T, List<ItemBean>> {
  @override
  List<ItemBean> get(T state) {
    if (state.rooms?.isNotEmpty == true) {
      return state.rooms
          .map<ItemBean>((RoomState data) => ItemBean('room', data))
          .toList(growable: true);
    } else {
      return <ItemBean>[];
    }
  }

  @override
  void set(T state, List<ItemBean> toDos) {
    if (toDos?.isNotEmpty == true) {
      state.rooms = List<RoomState>.from(
          toDos.map<RoomState>((ItemBean bean) => bean.data).toList());
    } else {
      state.rooms = [];
    }
  }

  @override
  subReducer(reducer) {
    return null;
  }

  @override
  Dependent<T> operator +(AbstractLogic<List<ItemBean>> logic) {
    // TODO: implement +
    return null;
  }
}