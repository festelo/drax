
import 'package:fish_redux/fish_redux.dart';

enum RoomAction{
  open
}

class RoomActionCreate{
  static Action open() => const Action(RoomAction.open);
}