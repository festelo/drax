import 'package:fish_redux/fish_redux.dart' as fish;
import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/widgets.dart';

import '../../../shared_states.dart';
import 'action.dart';
import '../../../router.dart';

Effect<RoomState> buildEffect(){
  return combineEffects({
      RoomAction.open: _open,
  });
}

void _open(fish.Action action, Context<RoomState> context){
  final route = Routes.room(roomId: context.state.id);
  Navigator
    .of(context.context)
    .pushNamed(route);
}