import 'package:fish_redux/fish_redux.dart';

import 'view.dart';
import 'effect.dart';
import '../../../shared_states.dart';

class RoomComponent extends Component<RoomState> {
  RoomComponent()
      : super(
          view: buildView,
          effect: buildEffect(),
        );
}