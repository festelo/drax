
import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import '../../../shared_states.dart';
import '../../../components.dart';
import 'action.dart';

Widget buildView(RoomState state, Dispatch dispatch, ViewService viewService) { 
  return Padding( 
    padding: EdgeInsets.only(bottom: 20),
    child: FlatButton(
      padding: EdgeInsets.all(0),
      child: RoomCard(
        background: state.localImage ? AssetImage(state.imageUrl) : NetworkImage(state.imageUrl),
        text: state.name,
      ),
      onPressed: () => dispatch(RoomActionCreate.open()),
    ) 
  );
}