import 'package:fish_redux/fish_redux.dart';

import 'action.dart';
import 'state.dart';

Reducer<HomeState> buildReducer() {
  return asReducer<HomeState>(<Object, Reducer<HomeState>>{
    HomeAction.switchPage: _switchPage,
    HomeAction.refreshSilent: _refreshSilent,
    HomeAction.setLoading: _setLoading,
  });
}

HomeState _refreshSilent(HomeState state, Action action) {
  return state.clone()
    ..rooms = action.payload['rooms']
    ..user = action.payload['user'];
}

HomeState _switchPage(HomeState state, Action action) {
  return state.clone()
    ..currentPage = action.payload;
}

HomeState _setLoading(HomeState state, Action action) {
  return state.clone()
    ..loading = action.payload;
}