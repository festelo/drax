import 'package:fish_redux/fish_redux.dart';

import '../shared_states.dart';
import 'all_rooms_tab/state.dart';
import 'all_devices_tab/state.dart';
import 'favorites_tab/state.dart';
import '../mixins.dart';

class HomeState with AuthState, DatabaseState implements Cloneable<HomeState> {
  AllDevicesTabState allDevicesTabState;
  List<RoomState> rooms;
  UserState user;
  int currentPage;
  bool loading;

  @override
  HomeState clone() {
    return HomeState()
      .. rooms = rooms
      .. user  = user
      .. currentPage = currentPage
      .. loading = loading
      .. allDevicesTabState = allDevicesTabState;
  }
}

class AllRoomsTabConnector extends ConnOp<HomeState, AllRoomsTabState> {
  @override
  AllRoomsTabState get(HomeState state) {
    return AllRoomsTabState()
      ..rooms = state.rooms ?? []
      ..user = state.user;
  }

  @override
  void set(HomeState state, AllRoomsTabState subState) {}
}

class FavoritesTabConnector extends ConnOp<HomeState, FavoritesTabState> {
  @override
  FavoritesTabState get(HomeState state) {
    return FavoritesTabState()
      ..rooms = state.rooms?.where((r) => r.favorite)?.toList() ?? []
      ..user = state.user;
  }

  @override
  void set(HomeState state, FavoritesTabState subState) {}
}

class AllDevicesTabConnector extends ConnOp<HomeState, AllDevicesTabState> {
  @override
  AllDevicesTabState get(HomeState state) {
    return state.allDevicesTabState.clone()
      ..rooms = state.rooms?.toList() ?? []
      ..database = state.database;
  }

  @override
  void set(HomeState state, AllDevicesTabState subState) {
    state.allDevicesTabState = subState;
  }
}

HomeState initState(String userId) {
  final HomeState state = HomeState();
  state.user = UserState()..id = userId;
  state.currentPage = 1;
  state.loading = true;
  state.allDevicesTabState = AllDevicesTabState.initState();
  return state;
}