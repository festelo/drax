import 'package:fish_redux/fish_redux.dart';

import '../rooms_tab.dart';
import 'state.dart';
import 'view.dart';
import 'effect.dart';
import 'reducer.dart';
import 'devices_adapter.dart';
import 'rooms/component.dart';

class AllDevicesTabComponent extends Component<AllDevicesTabState> {
  AllDevicesTabComponent()
      : super(
          view: buildView,
          effect: buildEffect(),
          reducer: buildReducer(),
          dependencies: Dependencies(
            adapter: NoneConn() + DeviceListAdapter(),
            slots: {
              'rooms': RoomsStateConnector() + RoomsListComponent()
            }
          )
        );
}