import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/widgets.dart';
import '../../shared_states.dart';
import '../../mixins.dart';
import '../rooms_tab.dart';
import 'rooms/state.dart';
import '../../device/state.dart';

class AllDevicesTabState with DatabaseState implements Cloneable<AllDevicesTabState> {
  List<RoomState> rooms;
  List<DeviceComponentState> selectedRoomDevices;
  RoomState selectedRoom;
  bool loading;

  AllDevicesTabState();
  AllDevicesTabState.initState() : loading = false;


  @override
  AllDevicesTabState clone() {
    return AllDevicesTabState()
      .. rooms = rooms
      .. selectedRoom = selectedRoom
      .. selectedRoomDevices = selectedRoomDevices
      .. loading = loading;
  }
}

class RoomsStateConnector extends ConnOp<AllDevicesTabState, RoomsState> {
  @override
  RoomsState get(AllDevicesTabState state) {
    return RoomsState()
      ..rooms = state.rooms
      ..selectedRoom = state.selectedRoom
      ..selectable = !state.loading;
  }

  @override
  void set(AllDevicesTabState state, RoomsState rooms) {
    state.selectedRoom = rooms.selectedRoom;
  }
}