import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'state.dart';

Widget buildView(RoomsState state, Dispatch dispatch, ViewService viewService) { 
  final ListAdapter adapter = viewService.buildAdapter();
  return ListView.builder(
    scrollDirection: Axis.horizontal,
    itemCount: adapter.itemCount,
    itemBuilder: adapter.itemBuilder
  );
}