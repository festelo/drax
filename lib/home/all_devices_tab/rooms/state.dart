import 'package:fish_redux/fish_redux.dart';
import '../../../shared_states.dart';

class RoomsState implements Cloneable<RoomsState> {
  List<RoomState> rooms;
  RoomState selectedRoom;
  bool selectable;

  @override
  RoomsState clone() {
    return RoomsState()
      .. rooms = rooms
      .. selectedRoom = selectedRoom
      .. selectable = selectable;
  }
}