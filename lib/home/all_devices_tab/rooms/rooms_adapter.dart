import 'package:fish_redux/fish_redux.dart';

import '../../../shared_states.dart';
import 'state.dart';
import 'room_button/component.dart';
import 'room_button/state.dart';

class RoomListAdapter extends DynamicFlowAdapter<RoomsState> {
  RoomListAdapter()
      : super(
          pool: <String, Component<Object>>{
            'room': RoomButtonComponent(),
          },
          connector: _RoomListConnector(),
          reducer: (a, b) { print(b.type); return a; },
        );
}

class _RoomListConnector extends ConnOp<RoomsState, List<ItemBean>> {
  @override
  List<ItemBean> get(RoomsState state) {
    if (state.rooms?.isNotEmpty == true) {
      return state.rooms
          .map<ItemBean>((data) => 
            ItemBean('room', 
              RoomButtonState()
                ..room = data
                ..isSelected = state.selectedRoom == data
                ..selectable = state.selectable
              )
          )
          .toList(growable: true);
    } else {
      return <ItemBean>[];
    }
  }
}