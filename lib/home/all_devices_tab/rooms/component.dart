import 'package:fish_redux/fish_redux.dart';
import 'state.dart';
import 'rooms_adapter.dart';
import 'view.dart';

class RoomsListComponent extends Component<RoomsState> {
  RoomsListComponent()
      : super(
          view: buildView,
          dependencies: Dependencies(
            adapter: NoneConn() + RoomListAdapter()
          )
        );
}