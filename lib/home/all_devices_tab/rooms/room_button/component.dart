import '../../../../components.dart';
import '../../../../shared_states.dart';
import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'view.dart';
import 'state.dart';

class RoomButtonComponent extends Component<RoomButtonState> {
  RoomButtonComponent()
      : super(
          view: buildView,
        );
}