import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import '../../../../components.dart';
import '../../../../shared_states.dart';
import 'state.dart';
import '../../action.dart';

Widget buildView(RoomButtonState state, Dispatch dispatch, ViewService viewService) { 
  return BoxButton(
    decoration: BoxDecoration(
      borderRadius: BorderRadius.only(
        topLeft: Radius.circular(10),
        topRight: Radius.circular(10),
      ),
      image: DecorationImage(
        fit: BoxFit.cover,
        alignment: Alignment.topLeft,
        image: state.room.localImage 
          ? AssetImage(state.room.imageUrl)
          : NetworkImage(state.room.imageUrl)
      )
    ),
    border: RoundedRectangleBorder(
      borderRadius: BorderRadius.only(
        topLeft: Radius.circular(10),
        topRight: Radius.circular(10),
      ),
      side: !state.isSelected 
        ? BorderSide.none
        : BorderSide(
          color: Color(0xFF00AEEF),
          width: 3
        )
    ),
    child: Container(
      width: double.infinity,
      height: double.infinity,
      child: Align(
        alignment: Alignment.bottomCenter,
        child: Container(
          width: double.infinity,
          padding: EdgeInsets.only(top: 4, bottom: 4),
          color: Color(0xAF000000),
          child: Text(state.room.name, textAlign: TextAlign.center),
        )
      )
    ),
    color: Colors.transparent,
    onPressed: !state.selectable ? null : () => dispatch(AllDevicesActionCreate.selectRoom(state.room)),
  );
}