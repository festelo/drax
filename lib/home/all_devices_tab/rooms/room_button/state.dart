import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/widgets.dart';
import '../../../../shared_states.dart';

class RoomButtonState implements Cloneable<RoomButtonState> {
  RoomState room;
  bool isSelected;
  bool selectable;

  @override
  RoomButtonState clone() {
    return RoomButtonState()
      .. room = room
      .. isSelected = isSelected;
  }
}