import '../../shared_states.dart';
import 'package:fish_redux/fish_redux.dart';
import '../../device/state.dart';

enum AllDevicesAction{
  displayDevices,
  selectRoom,
  selectRoomSilent,
  setLoading
}

class AllDevicesActionCreate{
  static Action setLoading(bool state) => Action(AllDevicesAction.setLoading, payload: state);
  static Action selectRoom(RoomState state) => Action(AllDevicesAction.selectRoom, payload: state);
  static Action selectRoomSilent(RoomState state) => Action(AllDevicesAction.selectRoomSilent, payload: state);
  static Action displayDevices(List<DeviceComponentState> devices) => 
    Action(
      AllDevicesAction.displayDevices, 
      payload: {
        'devices' : devices
      }
    );
} 