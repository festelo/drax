import 'package:fish_redux/fish_redux.dart';

import '../../shared_states.dart';
import '../../device/state.dart';
import 'state.dart';
import 'action.dart';

Effect<AllDevicesTabState> buildEffect(){
  return combineEffects({
    AllDevicesAction.selectRoom: _selectRoom,
  });
}

Future<void> _selectRoom(Action action, Context<AllDevicesTabState> context) async {
  final state = context.state;
  RoomState room = action.payload;
  if(room != null) {
    context.dispatch(AllDevicesActionCreate.selectRoomSilent(room));
    context.dispatch(AllDevicesActionCreate.setLoading(true));
    final roomDoc = await state.database.rooms.document(room.id).get();
    final devices = await state.database.devices.getByRoomDocument(roomDoc);
    context.dispatch(AllDevicesActionCreate.displayDevices(devices.map((d) => DeviceComponentState()..device = d).toList()));
    context.dispatch(AllDevicesActionCreate.setLoading(false));
  } 
}