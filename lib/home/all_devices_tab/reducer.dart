import 'package:fish_redux/fish_redux.dart';

import 'action.dart';
import 'state.dart';

Reducer<AllDevicesTabState> buildReducer() {
  
  return combineReducers(
    [
       asReducer<AllDevicesTabState>({
        AllDevicesAction.displayDevices: _displayDevices,
        AllDevicesAction.selectRoomSilent: _selectRoomSilent,
        AllDevicesAction.setLoading: _setLoading,
      }),
      (a, b) { return a; },
    ]
  );
}

AllDevicesTabState _displayDevices(AllDevicesTabState state, Action action) {
  return state.clone()
    ..selectedRoomDevices = action.payload['devices'];
}

AllDevicesTabState _selectRoomSilent(AllDevicesTabState state, Action action) {
  return state.clone()
    ..selectedRoom = action.payload;
}

AllDevicesTabState _setLoading(AllDevicesTabState state, Action action) {
  return state.clone()
    ..loading = action.payload;
}