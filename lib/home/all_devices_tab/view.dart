import 'dart:ui';

import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

import 'state.dart';
import '../../components.dart';
import '../../device/view.dart' as d;
import '../../shared_states.dart';

Widget buildView(AllDevicesTabState state, Dispatch dispatch, ViewService viewService) {
  ListAdapter _adapter;
  devicesAdapter() => _adapter == null ? _adapter = viewService.buildAdapter() : _adapter;
  final rooms = viewService.buildComponent('rooms');
  return Container(
    padding: const EdgeInsets.symmetric(horizontal: 00, vertical: 00),
    decoration: BoxDecoration(
      image: DecorationImage(
        fit: BoxFit.cover,
        alignment: Alignment.topLeft,
        colorFilter: new ColorFilter.mode(Color(0xFF373447), BlendMode.dstATop),
        image: AssetImage('assets/backs/all_devices.png')
      )
    ),
    child: Column(  
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        Center(
          child: Padding(
            padding: EdgeInsets.only(top: 70),
            child: Text('All devices', style: TextStyle(fontSize: 40, fontWeight: FontWeight.w600),)
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: 30, left: 20, right: 20),
          child: Text('Rooms', style: TextStyle(fontSize: 25, fontWeight: FontWeight.w600),),
        ),
        Container(
          height: 127,
          margin: EdgeInsets.only(top: 20),
          child: rooms 
        ),
        Expanded(
          child: Container(
            width: double.infinity,
            margin: EdgeInsets.only(top: 20),
            padding: EdgeInsets.symmetric(vertical: 20),
            color: Color(0x90FFFFFF),
            child: state.loading 
              ? Center(child: CircularProgressIndicator())
              : GridView.builder(
                scrollDirection: Axis.horizontal,
                gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                  maxCrossAxisExtent: 170
                ),
                itemCount: devicesAdapter().itemCount,
                itemBuilder: devicesAdapter().itemBuilder
              ),
          ),
        )
      ],
    )
  );
}