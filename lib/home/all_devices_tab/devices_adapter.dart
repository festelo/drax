import 'package:fish_redux/fish_redux.dart';

import '../../shared_states.dart';
import '../../device/component.dart';
import 'state.dart';

class DeviceListAdapter extends DynamicFlowAdapter<AllDevicesTabState> {
  DeviceListAdapter()
      : super(
          pool: <String, Component<Object>>{
            'device': DeviceComponent(),
          },
          connector: _DeviceListConnector(),
        );
}

class _DeviceListConnector extends ConnOp<AllDevicesTabState, List<ItemBean>> {
  @override
  List<ItemBean> get(AllDevicesTabState state) {
    if (state.selectedRoomDevices?.isNotEmpty == true) {
      return state.selectedRoomDevices
          .map<ItemBean>(( data) => ItemBean('device', data))
          .toList(growable: true);
    } else {
      return <ItemBean>[];
    }
  }

  @override
  void set(AllDevicesTabState state, List<ItemBean> devices) {
    if (devices?.isNotEmpty == true) {
      state.selectedRoomDevices = List<DeviceComponentState>.from(
          devices.map((bean) => bean.data).toList()
      );
    } else {
      state.selectedRoomDevices = [];
    }
  }
}