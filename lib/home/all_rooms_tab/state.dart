import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/widgets.dart';
import '../../shared_states.dart';
import '../rooms_tab.dart';

class AllRoomsTabState implements RoomsTabState, Cloneable<AllRoomsTabState> {
  List<RoomState> rooms;
  UserState user;

  @override
  AllRoomsTabState clone() {
    return AllRoomsTabState()
      .. rooms = rooms
      .. user  = user;
  }
}