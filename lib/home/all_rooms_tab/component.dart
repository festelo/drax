import 'package:fish_redux/fish_redux.dart';

import '../rooms_tab.dart';
import 'state.dart';
import 'view.dart';

class AllRoomsTabComponent extends Component<AllRoomsTabState> {
  AllRoomsTabComponent()
      : super(
          view: buildView,
          dependencies: Dependencies<AllRoomsTabState>(
              adapter: NoneConn() + RoomListAdapter<AllRoomsTabState>(),
          ),
        );
}