import 'package:fish_redux/fish_redux.dart';

import 'state.dart';
import 'view.dart';
import 'reducer.dart';
import 'effect.dart';

import 'all_rooms_tab/component.dart';
import 'all_devices_tab/component.dart';
import 'favorites_tab/component.dart';

class HomePage extends Page<HomeState, String> {
  HomePage()
      : super(
          initState: initState,
          view: buildView,
          effect: buildEffect(),
          reducer: buildReducer(),
          dependencies: Dependencies<HomeState>(
            slots: {
              'all_rooms_tab': AllRoomsTabConnector() + AllRoomsTabComponent(),
              'favorites_tab': FavoritesTabConnector() + FavoritesTabComponent(),
              'all_devices_tab': AllDevicesTabConnector() + AllDevicesTabComponent(),
            }
          ),
          middleware: [
            logMiddleware(tag: 'HomePage'),
          ],
        );
}