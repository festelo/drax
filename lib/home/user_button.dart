import 'package:drax/shared_states.dart';
import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';

import 'action.dart';

Widget userButton(UserState user, Dispatch dispatch) {
  return Container(
    width: 70,
    height: 70,
    child: FlatButton(
      shape: CircleBorder(),
      onPressed: () => dispatch(HomeActionCreate.openUserPage()),
      padding: EdgeInsets.all(0),
      child: ClipOval(
        child: Container(
          width: double.infinity,
          height: double.infinity,
          child: Image(
            fit: BoxFit.cover,
            image: user.photoUrl == null ? AssetImage('assets/demo/user-photo.png') : NetworkImage(user.photoUrl),
          ),
        )
      ) 
    ),
  );
}