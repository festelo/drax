import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';

import 'state.dart';
import '../action.dart';
import '../user_button.dart';

Widget buildView(
    FavoritesTabState state, Dispatch dispatch, ViewService viewService) {
  final ListAdapter adapter = viewService.buildAdapter();
  return LayoutBuilder(
    builder: (context, viewportConstraints) => SingleChildScrollView(
      child: Container(
        constraints: BoxConstraints(
          minHeight: viewportConstraints.maxHeight,
        ),
        padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 30),
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [
              Color(0xFF303147),
              Color(0xFF0C0C10),
              Color(0xFF14142D),
              Color(0xFF61617E),
            ],
            stops: [0, 0.067, 0.849, 1],
          ),
        ),
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.only(top: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    width: 70,
                  ),
                  Text(
                    'Favorites',
                    style: TextStyle(fontSize: 25, fontWeight: FontWeight.w700),
                  ),
                  Container(
                    width: 70,
                    child: IconButton(
                      padding: EdgeInsets.all(5),
                      alignment: Alignment.centerRight,
                      iconSize: 28,
                      icon: Icon(Icons.refresh),
                      onPressed: () => dispatch(HomeActionCreate.refresh()),
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  userButton(state.user, dispatch),
                  RichText(
                    text: TextSpan(
                      children: [
                        TextSpan(text: 'Welcome \n'),
                        TextSpan(
                            text: state.user.name,
                            style: TextStyle(color: Color(0xFFFFF704))),
                      ],
                      style:
                          TextStyle(fontSize: 20, fontWeight: FontWeight.w700),
                    ),
                    textAlign: TextAlign.center,
                  ),
                  Container(
                    width: 70,
                    height: 70,
                    child: IconButton(
                      padding: EdgeInsets.all(5),
                      alignment: Alignment.centerRight,
                      iconSize: 28,
                      icon: Icon(Icons.exit_to_app),
                      onPressed: () => dispatch(HomeActionCreate.logout()),
                    ),
                  )
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 10, bottom: 20),
              child: Divider(
                height: 10,
                color: Colors.white,
              ),
            ),
            ListView.builder(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                physics: NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                itemBuilder: adapter.itemBuilder,
                itemCount: adapter.itemCount),
          ],
        ),
      ),
    ),
  );
}
