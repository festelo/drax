import 'package:fish_redux/fish_redux.dart';

import '../rooms_tab.dart';
import 'state.dart';
import 'view.dart';

class FavoritesTabComponent extends Component<FavoritesTabState> {
  FavoritesTabComponent()
      : super(
          view: buildView,
          dependencies: Dependencies<FavoritesTabState>(
              adapter: NoneConn() + RoomListAdapter<FavoritesTabState>(),
          ),
        );
}