import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/widgets.dart';
import '../../shared_states.dart';
import '../rooms_tab.dart';

class FavoritesTabState implements RoomsTabState, Cloneable<FavoritesTabState> {
  List<RoomState> rooms;
  UserState user;

  @override
  FavoritesTabState clone() {
    return FavoritesTabState()
      .. rooms = rooms
      .. user  = user;
  }
}