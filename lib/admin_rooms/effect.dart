import 'package:fish_redux/fish_redux.dart';
import 'package:fish_redux/fish_redux.dart' as fish;
import 'package:flutter/material.dart';

import '../shared_effects.dart';
import '../router.dart';
import 'state.dart';
import 'action.dart';

Effect<AdminRoomsPageState> buildEffect() {
  final effects = {
    AdminRoomsAction.goBack: (a, ctx) => goBack(ctx.context),
    AdminRoomsAction.addRoom: _addRoom,
    AdminRoomsAction.refresh: _refresh,
    AdminRoomsAction.delete: _delete,
    Lifecycle.initState: _initState,
  };
  return combineEffects(effects);
}

Future<void> _delete(
    fish.Action action, Context<AdminRoomsPageState> context) async {
  String roomId = action.payload;
  final userId = context.state.user.id;
  await context.state.database.users
      .removeRoom(userId, context.state.database.rooms.document(roomId));
  await context.state.database.rooms.delete(roomId);
}

Future<void> _refresh(
    fish.Action action, Context<AdminRoomsPageState> context) async {
  if (context.state.user?.id != null) {
    final userDoc = await context.state.database.users
        .document(context.state.user.id)
        .get();
    final user = context.state.database.users.fromDocument(userDoc);
    final rooms = await context.state.database.rooms.getByUserDocument(userDoc);
    context.dispatch(AdminRoomsActionCreate.refreshSilent(user, rooms));
  }
}

void _initState(fish.Action action, Context<AdminRoomsPageState> context) {
  final id = context.state.user?.id;
  final db = context.state.database;
  if (id != null) {
    final sub = db.users.document(id).snapshots().listen((doc) async =>
        context.dispatch(AdminRoomsActionCreate.refreshSilent(
            db.users.fromDocument(doc),
            await db.rooms.getByUserDocument(doc))));
    context.onDisposed(() async {
      await sub.cancel();
      print('disposed');
    });
  }
  //context.dispatch(AdminRoomsActionCreate.refresh());
}

void _addRoom(fish.Action action, Context<AdminRoomsPageState> context) {
  final route = Routes.adminCreateRoom(userId: context.state.user.id);
  Navigator.of(context.context).pushNamed(route);
}
