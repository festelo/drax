import 'package:fish_redux/fish_redux.dart';

import '../../shared_states.dart';
import 'view.dart';
import 'effect.dart';

class RoomComponent extends Component<RoomState> {
  RoomComponent()
      : super(
          effect: buildEffect(),
          view: buildView,
        );
}