import 'dart:ui';

import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

import '../../components.dart';
import '../../shared_states.dart';
import 'action.dart';

Widget buildView(RoomState state, Dispatch dispatch, ViewService viewService) {
  return Container(
    decoration: BoxDecoration(
      border: Border.all(
        color: Colors.grey,
        width: 2
      ),
      color: Colors.grey,
    ),
    margin: EdgeInsets.symmetric(vertical: 7, horizontal: 4),
    child: Slidable(
      delegate: new SlidableDrawerDelegate(),
      actionExtentRatio: 0.25,
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(10)),
        ),
        child: FlatButton(
          padding: EdgeInsets.all(0),
          onPressed: () => dispatch(RoomActionCreate.openDevices()),
          child: RoomCard(
            paddingUnderTitle: 5,
            height: 100,
            background: state.localImage ? AssetImage(state.imageUrl) : NetworkImage(state.imageUrl),
            text: state.name,
          ),
        )
      ),
      secondaryActions: <Widget>[
        IconSlideAction(
          caption: 'Change',
          color: Colors.black45,
          icon: Icons.more_horiz,
          onTap: () => dispatch(RoomActionCreate.change()),
        ),
        IconSlideAction(
          caption: 'Delete',
          color: Colors.red,
          icon: Icons.delete,
          onTap: () => dispatch(RoomActionCreate.delete(state.id)),
        ),
      ],
    ),
  );
}