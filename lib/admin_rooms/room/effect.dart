import 'package:fish_redux/fish_redux.dart';
import 'package:fish_redux/fish_redux.dart' as fish;
import 'package:flutter/material.dart';

import '../../router.dart';
import '../../shared_states.dart';
import 'action.dart';

Effect<RoomState> buildEffect(){
  return combineEffects({
    RoomAction.openDevices: _openDevices,
    RoomAction.change: _change,
  });
}


void _openDevices(fish.Action action, Context<RoomState> context){
  final route = Routes.adminDevices(roomId: context.state.id);
  Navigator
    .of(context.context)
    .pushNamed(route);
}

void _change(fish.Action action, Context<RoomState> context){
  final route = Routes.adminChangeRoom(roomId: context.state.id);
  Navigator
    .of(context.context)
    .pushNamed(route);
}