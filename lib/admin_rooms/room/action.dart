import 'package:fish_redux/fish_redux.dart';

enum RoomAction{
  change,
  delete,
  openDevices,
}

class RoomActionCreate{
  static Action change() => const Action(RoomAction.change);
  static Action delete(String roomId) => Action(RoomAction.delete, payload: roomId);
  static Action openDevices() => const Action(RoomAction.openDevices);
}