import 'package:fish_redux/fish_redux.dart';

import '../shared_states.dart';
import 'room/component.dart';
import 'room/action.dart';
import 'state.dart';
import 'action.dart';

class RoomListAdapter extends DynamicFlowAdapter<AdminRoomsPageState>{
  RoomListAdapter()
      : super(
          pool: <String, Component<Object>>{
            'room': RoomComponent(),
          },
          effect: (action, context) {
            if(action.type == RoomAction.delete) {
              context.dispatch(AdminRoomsActionCreate.delete(action.payload));
            }
          },
          connector: _RoomsListConnector(),
        );
}

class _RoomsListConnector extends ConnOp<AdminRoomsPageState, List<ItemBean>> {
  @override
  List<ItemBean> get(AdminRoomsPageState state) {
    if (state.rooms?.isNotEmpty == true) {
      return state.rooms
          .map<ItemBean>((data) => ItemBean('room', data))
          .toList(growable: true);
    } else {
      return <ItemBean>[];
    }
  }

  @override
  void set(AdminRoomsPageState state, List<ItemBean> devices) {
    if (devices?.isNotEmpty == true) {
      state.rooms = List.from(
          devices.map((bean) => bean.data).toList()
      );
    } else {
      state.rooms = [];
    }
  }
}