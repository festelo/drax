import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

import '../components.dart';
import 'state.dart';
import 'action.dart';

Widget buildView(AdminRoomsPageState state, Dispatch dispatch, ViewService viewService) {
  final ListAdapter adapter = viewService.buildAdapter();
  return Scaffold(
    floatingActionButton: FloatingActionButton(
      backgroundColor: Colors.white,
      child: Icon(Icons.add, size: 35,  color: Colors.black,),
      onPressed: () => dispatch(AdminRoomsActionCreate.addRoom()),
    ),
    body: Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          fit: BoxFit.cover,
          alignment: Alignment.topLeft,
          colorFilter: new ColorFilter.mode(Color(0xFF373447), BlendMode.dstATop),
          image: AssetImage('assets/backs/login.png')
        )
      ),
      padding: const EdgeInsets.only(top: 16.0),
      child: CustomScrollView(
        slivers: <Widget>[
          SliverAppBar(
            backgroundColor: Colors.transparent,
            title: Text('Admin panel', style: TextStyle(fontSize: 35, fontWeight: FontWeight.w500),),
            actions: [
              Container(
                width: 30,
                margin: EdgeInsets.only(right: 10),
                child: IconButton(
                  padding: EdgeInsets.all(5),
                  alignment: Alignment.centerRight,
                  iconSize: 28,
                  icon: Icon(Icons.refresh),
                  onPressed: () => dispatch(AdminRoomsActionCreate.refresh()),
                ),
              ),
            ],
            bottom: PreferredSize(
              preferredSize: Size.fromHeight(70),
              child: Padding(
                padding: EdgeInsets.only(top: 10, bottom: 0),
                child: Column(
                  children: [
                    Text('Rooms', style: TextStyle(fontSize: 30, fontWeight: FontWeight.w500), textAlign: TextAlign.center),
                    Text('of ' + state.user?.name, style: TextStyle(fontSize: 15, fontWeight: FontWeight.w500), textAlign: TextAlign.center,),
                  ],
                ),
              )
            ),
          ),
          SliverPadding(
            padding: EdgeInsets.only(top: 20),
            sliver: SliverList(
              delegate: SliverChildBuilderDelegate(
                adapter.itemBuilder,
                childCount: adapter.itemCount,
              ),
            ),
          )
        ],
      )
    )
  );
}