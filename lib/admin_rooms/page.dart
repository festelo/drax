import 'package:fish_redux/fish_redux.dart';

import 'effect.dart';
import 'state.dart';
import 'view.dart';
import 'reducer.dart';
import 'rooms_adapter.dart';

class AdminRoomsPage extends Page<AdminRoomsPageState, String> {
  AdminRoomsPage()
      : super(
          initState: initState,
          effect: buildEffect(),
          reducer: buildReducer(),
          view: buildView,
          dependencies: Dependencies(
            adapter: NoneConn() + RoomListAdapter()
          ),
          middleware: [
            logMiddleware(tag: 'AdminRoomsPage'),
          ],
        );
}