import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/widgets.dart';

import '../shared_states.dart';
import 'page.dart';
import '../mixins.dart';

class AdminRoomsPageState with DatabaseState implements Cloneable<AdminRoomsPageState> {
  UserState user;
  List<RoomState> rooms;
  @override
  AdminRoomsPageState clone() {
    return AdminRoomsPageState()
      ..user = user
      ..rooms = rooms;
  }
}

AdminRoomsPageState initState(String userId) {
  final state = AdminRoomsPageState();
  state.user = UserState()..id = userId;
  return state;
}