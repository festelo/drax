import 'package:fish_redux/fish_redux.dart';

import 'action.dart';
import 'state.dart';
import '../shared_states.dart';

Reducer<AdminRoomsPageState> buildReducer() {
  return asReducer({
    AdminRoomsAction.refreshSilent: _refreshSilent,
  });
}

AdminRoomsPageState _refreshSilent(AdminRoomsPageState state, Action action) {
  UserState user = action.payload['user'];
  List<RoomState> rooms = action.payload['rooms'];
  return state.clone()
    ..user = user
    ..rooms = rooms;
}
