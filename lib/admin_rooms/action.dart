import 'package:fish_redux/fish_redux.dart';
import '../shared_states.dart';

enum AdminRoomsAction{
  addRoom,
  goBack,
  delete,
  refreshSilent,
  refresh
}

class AdminRoomsActionCreate{
  static Action addRoom() => const Action(AdminRoomsAction.addRoom);
  static Action goBack() => const Action(AdminRoomsAction.goBack);
  static Action refresh() => const Action(AdminRoomsAction.refresh);
  static Action delete(String roomId) => Action(AdminRoomsAction.delete, payload: roomId);
  static Action refreshSilent(UserState user, List<RoomState> rooms) => 
    Action(AdminRoomsAction.refreshSilent, payload: {'rooms': rooms, 'user': user});
}