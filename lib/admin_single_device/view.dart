import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

import '../components.dart';
import 'state.dart';
import 'action.dart';

Widget buildView(AdminSingleDevicePageState state, Dispatch dispatch, ViewService viewService) {
  return Scaffold(
    key: state.scaffoldKey,
    body: Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          fit: BoxFit.cover,
          alignment: Alignment.topLeft,
          colorFilter: new ColorFilter.mode(Color(0xFF373447), BlendMode.dstATop),
          image: AssetImage('assets/backs/login.png')
        )
      ),
      padding: const EdgeInsets.symmetric(horizontal: 0.0),
      child: state.loading 
        ? Center(child:  CircularProgressIndicator(),)
        : CustomScrollView( 
          slivers: [
            SliverAppBar(
              centerTitle: true,
              backgroundColor: Colors.transparent,
              title: Text('Admin panel', style: TextStyle(fontSize: 35, fontWeight: FontWeight.w500),),
            ),
            SliverPadding(
              padding: EdgeInsets.symmetric(horizontal: 16),
              sliver: SliverList(
                delegate: SliverChildListDelegate(
                  [
                    Padding(
                      padding: EdgeInsets.only(top: 20),
                      child: Text('Device info', style: TextStyle(fontSize: 30, fontWeight: FontWeight.w500), textAlign: TextAlign.center,),
                    ),
                    FlatButton(
                      child: Text(state.device.id ?? 'Not saved device'),
                      onPressed: state.device.id == null 
                        ? null 
                        : () => dispatch(AdminSingleDeviceActionCreate.saveId()),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 20),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                        border: Border.all(
                          color: Colors.grey,
                          width: 2
                        )
                      ),
                      child: Column(
                        children: [
                          DraxTextField(
                            leftIcon: Icon(Icons.tag_faces), 
                            controller: state.nameController,
                            hintText: 'Device name',
                          ),
                          DraxTextField(
                            leftIcon: Icon(Icons.link), 
                            controller: state.onController,
                            hintText: 'On URL',
                          ),
                          DraxTextField(
                            leftIcon: Icon(Icons.link_off), 
                            controller: state.offController,
                            hintText: 'Off URL',
                          ),
                          SwitchListTile(
                            title: Text('Stretch image', style: TextStyle(color: Colors.white),),
                            activeColor: Colors.white,
                            onChanged: (val) => dispatch(AdminSingleDeviceActionCreate.refreshSilent(filledImage: !state.device.filledImage)),
                            value: state.device.filledImage,
                          ),
                          RectButton(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.vertical(bottom: Radius.circular(10)),
                              border: Border.all(width: 1)
                            ),
                            text: 'Save',
                            margin: EdgeInsets.only(top: 1),
                            onPressed: () => dispatch(AdminSingleDeviceActionCreate.changeInfo()),
                          )
                        ],
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 20),
                      child: FlatButton(
                        child: Text('Custom URLs'),
                        onPressed: () => dispatch(AdminSingleDeviceActionCreate.openCustomActions()),
                      ),
                    ),
                    RectButton(
                      decoration: BoxDecoration(
                        border: Border.all(color: Colors.grey, width: 2)
                      ),
                      margin: EdgeInsets.only(top: 20),
                      text: 'Transfer to another room',
                      onPressed: () => dispatch(AdminSingleDeviceActionCreate.showChangeRoomDialog()),
                    ),
                    RectButton(
                      decoration: BoxDecoration(
                        border: Border.all(color: Colors.grey, width: 2)
                      ),
                      margin: EdgeInsets.only(top: 10),
                      text: 'Change photo',
                      onPressed: () => dispatch(AdminSingleDeviceActionCreate.changePhoto()),
                    )
                  ]
                ),
              ) 
            ),
          ]
        )
    )
  );
}