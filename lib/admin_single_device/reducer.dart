import 'package:fish_redux/fish_redux.dart';
import 'package:fish_redux/fish_redux.dart' as fish;
import 'package:flutter/widgets.dart';

import '../shared_states.dart';
import 'action.dart';
import 'state.dart';

Reducer<AdminSingleDevicePageState> buildReducer() {
  return asReducer({
    AdminSingleDeviceAction.refreshSilent: _refreshSilent,
    AdminSingleDeviceAction.setLoading: _setLoading,
  });
}

AdminSingleDevicePageState _refreshSilent(AdminSingleDevicePageState state, fish.Action action) {
  DeviceState device = action.payload['device'] ?? state.device;
  List<RoomState> rooms = action.payload['rooms'] ?? state.allRooms;
  final filledImage = action.payload['filledImage'] ?? device?.filledImage;
  device?.filledImage = filledImage;
  return state.clone()
    ..device = device
    ..allRooms = rooms
    
    ..nameController = state.device?.name == device?.name 
      ? state.nameController
      : TextEditingController(text: device?.name)

    ..onController = state.device?.onUrl == device?.onUrl 
      ? state.onController
      : TextEditingController(text: device?.onUrl)

    ..offController = state.device?.offUrl == device?.offUrl 
      ? state.offController
      : TextEditingController(text: device?.offUrl);
}

AdminSingleDevicePageState _setLoading(AdminSingleDevicePageState state, fish.Action action) {
  return state.clone()
    ..loading = action.payload;
}