import 'package:fish_redux/fish_redux.dart';
import 'package:fish_redux/fish_redux.dart' as fish;
import 'package:flutter/material.dart';

import '../shared_states.dart';
import '../shared_effects.dart';
import '../router.dart';
import 'state.dart';
import 'action.dart';

Effect<AdminSingleDevicePageState> buildEffect(){
  return combineEffects({
    AdminSingleDeviceAction.saveId: (a, c) => saveToClipboard(c.state.device.id),
    AdminSingleDeviceAction.changeInfo: _changeInfo,
    AdminSingleDeviceAction.changePhoto: _changePhoto,
    AdminSingleDeviceAction.changeRoom: _changeRoom,
    AdminSingleDeviceAction.showChangeRoomDialog: _showChangeRoomDialog,
    AdminSingleDeviceAction.openCustomActions: _openCustomActions,
    AdminSingleDeviceAction.refresh: _refresh,
    Lifecycle.initState: _initState
  });
}

void _initState(fish.Action action, Context<AdminSingleDevicePageState> context) {
  context.dispatch(AdminSingleDeviceActionCreate.refresh());
}

Future<void> _refresh(fish.Action action, Context<AdminSingleDevicePageState> context) async {
  final id = action.payload['id'] ?? context.state.device?.id;
  DeviceState device;
  if(id != null) {
    device = await context.state.database.devices.get(id);
  }
  else device = DeviceState();
  final rooms = await context.state.database.rooms.getAll();
  context.dispatch(AdminSingleDeviceActionCreate.refreshSilent(rooms: rooms, device: device));
  context.dispatch(AdminSingleDeviceActionCreate.setLoading(false));
}

Future<void> _changeRoom(fish.Action action, Context<AdminSingleDevicePageState> context) async {
  final newId = action.payload;
  context.dispatch(AdminSingleDeviceActionCreate.setLoading(true));

  final deviceId = context.state.device.id;
  final db = context.state.database;

  final deviceDoc = db.devices.document(deviceId);
  final roomDoc = await db.devices.getOwnerDocument(deviceId);

  db.rooms.removeDevice(roomDoc.documentID, deviceDoc);
  db.rooms.addDevice(newId, deviceDoc);

  await db.devices.update(deviceId, {'owner': db.rooms.document(newId)});

  context.dispatch(AdminSingleDeviceActionCreate.setLoading(false));
}

void _showChangeRoomDialog(fish.Action action, Context<AdminSingleDevicePageState> context){
  if(context.state.device.id == null) {
    showSnackMessage(context.state.scaffoldKey.currentState, 'You should to set info first');
    return;
  }
  else {
    showCustomDialog(context.context, (buildContext) =>
      context.state.allRooms.map((l) => ListTile( 
        title: Text(l.name),
        onTap: () { 
          context.dispatch(AdminSingleDeviceActionCreate.changeRoom(l.id));
          Navigator.pop(buildContext);
        },
      )
    ).toList());
  }
}

Future<void> _changePhoto(fish.Action action, Context<AdminSingleDevicePageState> context) async {
  final db = context.state.database;
  final storage = context.state.storage;
  final device = context.state.device;
  if(device.id == null) {
    showSnackMessage(context.state.scaffoldKey.currentState, 'You should to set info first');
    return;
  }
  else {
    final file = await showChangePhotoDialog(context.context);
    if(file == null) return;
    context.dispatch(AdminSingleDeviceActionCreate.setLoading(true));
    final url = await storage.upload(file, 'device_${device.id}');
    await db.devices.update(device.id, {
      'imageUrl': url,
      'localImage': false
    });
    context.dispatch(AdminSingleDeviceActionCreate.refresh());
    context.dispatch(AdminSingleDeviceActionCreate.setLoading(false));
  }
}

Future<void> _changeInfo(fish.Action action, Context<AdminSingleDevicePageState> context) async {
  final oldDevice = context.state.device.clone();
  var id = oldDevice.id;
  final db = context.state.database;

  oldDevice.name = context.state.nameController.text;
  oldDevice.onUrl = context.state.onController.text;
  oldDevice.offUrl = context.state.offController.text;

  context.dispatch(AdminSingleDeviceActionCreate.setLoading(true));
  if(id != null) {
    await db.devices.update(oldDevice.id, oldDevice.toMap());
  }
  else {
    final roomId = context.state.roomId;
    id = await db.devices.create(oldDevice, additional: {
      'owner': context.state.database.rooms.document(roomId)
    });
    await db.rooms.addDevice(roomId, db.devices.document(id));
  }
  context.dispatch(AdminSingleDeviceActionCreate.refresh(newId: id));
  context.dispatch(AdminSingleDeviceActionCreate.setLoading(false));
}

void _openCustomActions(fish.Action action, Context<AdminSingleDevicePageState> context){
  if(context.state.device.id == null) {
    showSnackMessage(context.state.scaffoldKey.currentState, 'You should to set info first');
    return;
  }
  final route = Routes.adminActions(deviceId: context.state.device.id);
  Navigator
    .of(context.context)
    .pushNamed(route);
}