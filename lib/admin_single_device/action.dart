import 'package:fish_redux/fish_redux.dart';
import '../shared_states.dart';

enum AdminSingleDeviceAction{
  changePhoto,
  changeInfo,
  changeRoom,
  showChangeRoomDialog,
  refreshSilent,
  openCustomActions,
  setLoading,
  refresh,
  saveId
}

class AdminSingleDeviceActionCreate{
  static Action showChangeRoomDialog() => const Action(AdminSingleDeviceAction.showChangeRoomDialog);
  static Action changeInfo() => const Action(AdminSingleDeviceAction.changeInfo);
  static Action changePhoto() => const Action(AdminSingleDeviceAction.changePhoto);
  static Action openCustomActions() => const Action(AdminSingleDeviceAction.openCustomActions);
  static Action changeRoom(String id) => Action(AdminSingleDeviceAction.changeRoom, payload: id);
  static Action setLoading(bool value) => Action(AdminSingleDeviceAction.setLoading, payload: value);
  static Action refresh({String newId}) => Action(AdminSingleDeviceAction.refresh, payload: {'id': newId});
  static Action refreshSilent({List<RoomState> rooms, DeviceState device, bool filledImage}) => 
    Action(
      AdminSingleDeviceAction.refreshSilent,
      payload: {
        'rooms': rooms,
        'device': device,
        'filledImage': filledImage
      }
    );
  static Action saveId() => const Action(AdminSingleDeviceAction.saveId);
}