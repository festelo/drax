import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'page.dart';
import '../shared_states.dart';
import '../mixins.dart';

class AdminSingleDevicePageState with DatabaseState, StorageState implements Cloneable<AdminSingleDevicePageState> {
  GlobalKey<State<Scaffold>> scaffoldKey;

  TextEditingController nameController;
  TextEditingController onController;
  TextEditingController offController;
  DeviceState device;
  bool loading;
  String roomId;

  List<RoomState> allRooms;

  @override
  AdminSingleDevicePageState clone() {
    return AdminSingleDevicePageState()
      ..scaffoldKey = scaffoldKey
      ..roomId = roomId
      ..loading = loading
      ..allRooms = allRooms
      ..nameController = nameController
      ..onController = onController
      ..offController = offController
      ..device = device;
  }
}

AdminSingleDevicePageState initState(AdminSingleDevicePageParams params) {
  final state = AdminSingleDevicePageState();
  state.scaffoldKey = GlobalKey();
  state.nameController = TextEditingController();
  state.onController = TextEditingController();
  state.offController = TextEditingController();
  state.loading = true;
  state.device = DeviceState()..id = params.deviceId;
  state.roomId = params.roomId;
  return state;
}