import 'package:fish_redux/fish_redux.dart';

import 'effect.dart';
import 'state.dart';
import 'view.dart';
import 'reducer.dart';

class AdminSingleDevicePageParams {
  final String deviceId;
  final String roomId;
  AdminSingleDevicePageParams.createNew(this.roomId) : deviceId = null;
  AdminSingleDevicePageParams.change(this.deviceId) : roomId = null;
}

class AdminSingleDevicePage extends Page<AdminSingleDevicePageState, AdminSingleDevicePageParams> {
  AdminSingleDevicePage()
      : super(
          initState: initState,
          effect: buildEffect(),
          reducer: buildReducer(),
          view: buildView,
          middleware: [
            logMiddleware(tag: 'AdminSingleDevicePage'),
          ],
        );
}