import 'package:fish_redux/fish_redux.dart';
import '../shared_states.dart';

class DeviceState implements DatabaseMappedState, Cloneable<DeviceState> {
  String id;
  String name;
  String imageUrl = 'assets/devices/light.png';
  String onUrl;
  String offUrl;
  Map<String,String> customActions = {};
  bool localImage = true;
  bool enabled = true;
  bool filledImage = false;

  DeviceState();
  DeviceState.fromMap(String id, Map<String, dynamic> map) :
    id = id,
    name = map['name'],
    onUrl = map['onUrl'],
    offUrl = map['offUrl'],
    imageUrl = map['imageUrl'],
    customActions = map['customActions'] != null 
      ? Map.from(map['customActions'])
      : <String, String>{},
    localImage = map['localImage'] == true,
    enabled = map['enabled'] == true,
    filledImage = map['filledImage'] == true;

  Map<String, dynamic> toMap(){
    return {
      'name': name,
      'imageUrl': imageUrl,
      'customActions': customActions,
      'localImage': localImage,
      'enabled': enabled,
      'onUrl': onUrl,
      'offUrl': offUrl,
      'filledImage': filledImage
    };
  }

  @override
  DeviceState clone() {
    return DeviceState()
      .. id = id
      .. name = name
      .. imageUrl = imageUrl
      .. customActions = customActions
      .. localImage = localImage
      .. enabled = enabled
      .. filledImage = filledImage;
  }
}