import '../shared_states.dart';

class RoomState implements DatabaseMappedState {
  String id;
  String name;
  String imageUrl = 'assets/rooms/bathroom.png';
  bool localImage = true;
  bool favorite = false;
  bool enabled = true;
  
  RoomState();
  RoomState.fromMap(String id, Map<String, dynamic> map) :
    id = id,
    name = map['name'],
    imageUrl = map['imageUrl'],
    localImage = map['localImage'],
    favorite = map['favorite'],
    enabled = map['enabled'];

  Map<String, dynamic> toMap(){
    return {
      'name': name,
      'imageUrl': imageUrl,
      'localImage': localImage,
      'favorite': favorite,
      'enabled': enabled
    };
  }

  @override
  RoomState clone() {
    return RoomState()
      .. id = id
      .. name = name
      .. imageUrl = imageUrl
      .. favorite = favorite
      .. localImage = localImage
      .. enabled = enabled;
  }
}