abstract class DatabaseMappedState {
  String id;
  Map<String, dynamic> toMap();
}