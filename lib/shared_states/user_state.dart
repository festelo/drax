import '../shared_states.dart';

class UserState implements DatabaseMappedState {
  String id;
  String name = 'Loading...';
  String photoUrl;

  UserState() {}
  UserState.fromMap(String id, Map<String, dynamic> map) : 
    id = id,
    name = map['name'],
    photoUrl = map['photoUrl'];

  Map<String, dynamic> toMap(){
    return {
      'name': name,
      'photoUrl': photoUrl,
    };
  }
}