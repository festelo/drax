import 'package:fluro/fluro.dart';
import 'package:flutter/widgets.dart' hide Router;
import '../router.dart';
import '../mixins.dart';

class AppState with AuthState, DatabaseState {
  Router router;
  GlobalKey<NavigatorState> navigator;

  static AppState initState() {
    final router = Router();
    Routes.configureRoutes(router);
    return AppState()
      ..router = router
      ..navigator = GlobalKey();
  }
}
