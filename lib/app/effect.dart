import 'package:fish_redux/fish_redux.dart';
import 'package:fish_redux/fish_redux.dart' as fish;
import 'package:flutter/material.dart';

import '../shared_effects.dart';
import '../router.dart';
import 'state.dart';
import 'action.dart';

Effect<AppState> buildEffect(){
  return combineEffects({
    AppAction.checkLoginState: _checkLoginState,
    AppAction.logOutSilent: _logOutSilent,
    AppAction.logInSilent: _logInSilent,
    AppAction.logInAdminSilent: _logInAdminSilent,
    Lifecycle.initState: _initState
  });
}

void _initState(fish.Action action, Context<AppState> context){
  context.state.auth.init(
    loginedOut: () => context.dispatch(AppActionCreate.logOutSilent()),
    //loginedIn: () => context.dispatch(AppActionCreate.logInSilent())
  );
  context.dispatch(AppActionCreate.checkLoginState());
}

Future<void> _checkLoginState(fish.Action action, Context<AppState> context) async {
  final authUser = await context.state.auth.getFreshUser();
  if(authUser != null && await context.state.database.users.exist(authUser.uid)) {
    final isAdmin = await context.state.database.users.isAdmin(authUser.uid);
    if(isAdmin) {
      context.dispatch(AppActionCreate.logInAdminSilent());
    } else {
      context.dispatch(AppActionCreate.logInSilent());
    }
  }
  else {
    context.dispatch(AppActionCreate.logOutSilent());
  }
}

void _logOutSilent(fish.Action action, Context<AppState> context){
  final route = Routes.login();
  context.state.navigator.currentState
    .pushNamedAndRemoveUntil(route, (a) => false);
}

void _logInSilent(fish.Action action, Context<AppState> context){
  logInSilent(context.state.auth, context.state.navigator.currentState);
}

void _logInAdminSilent(fish.Action action, Context<AppState> context){
  logInAdminSilent(context.state.navigator.currentState);
}