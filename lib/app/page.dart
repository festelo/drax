import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart' hide Page;
import 'theme.dart';
import '../router.dart';
import 'state.dart';
import 'effect.dart';

class AppPage extends Page<AppState, void> {
  AppPage()
      : super(
          initState: (_) => AppState.initState(),
          effect: buildEffect(),
          view: _buildView,
          middleware: [
            logMiddleware(tag: 'AppPage'),
          ],
        );

  static Widget _buildView(AppState state, dispatch, ViewService service) =>
      MaterialApp(
        title: 'Drax',
        theme: getTheme(),
        onGenerateRoute: state.router.generator,
        initialRoute: Routes.loading(),
        navigatorKey: state.navigator,
      );
}
