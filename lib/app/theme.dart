import 'package:flutter/material.dart';

const TextStyle _baseTextStyle = TextStyle(
  fontSize: 14,
  fontFamily: 'Montserrat'
);

ThemeData getTheme() {
  return ThemeData(
    brightness: Brightness.dark,
    primaryColor: Colors.lightBlue[800],
    accentColor: Colors.cyan[600],
    
    // Define the default Font Family
    fontFamily: 'Montserrat',
    
    // Define the default TextTheme. Use this to specify the default
    // text styling for headlines, titles, bodies of text, and more.
    textTheme: TextTheme(
      headline: _baseTextStyle.copyWith(fontWeight: FontWeight.bold),
      title: _baseTextStyle.copyWith(fontSize: 36.0, fontStyle: FontStyle.italic),
      body1: _baseTextStyle.copyWith(fontSize: 14.0),
      button:_baseTextStyle,
    ),
  );
}