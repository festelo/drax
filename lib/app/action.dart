import 'package:fish_redux/fish_redux.dart';

enum AppAction{
  checkLoginState,
  logOutSilent,
  logInSilent,
  logInAdminSilent
}

class AppActionCreate{
  static Action checkLoginState() => const Action(AppAction.checkLoginState);
  static Action logOutSilent() => const Action(AppAction.logOutSilent);
  static Action logInSilent() => const Action(AppAction.logInSilent);
  static Action logInAdminSilent() => const Action(AppAction.logInAdminSilent);
}