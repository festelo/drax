import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

import '../components.dart';
import 'state.dart';
import 'action.dart';


Widget buildView(AdminDevicesPageState state, Dispatch dispatch, ViewService viewService) {
  final ListAdapter adapter = viewService.buildAdapter();
  return Scaffold(
    body: Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          fit: BoxFit.cover,
          alignment: Alignment.topLeft,
          colorFilter: new ColorFilter.mode(Color(0xFF373447), BlendMode.dstATop),
          image: AssetImage('assets/backs/login.png')
        )
      ),
      padding: const EdgeInsets.only(top: 16.0),
      child: Column(
        children: [
          AppBar(
            backgroundColor: Colors.transparent,
            elevation: 0,
            title: Text('Admin panel', style: TextStyle(fontSize: 35, fontWeight: FontWeight.w500),),
            actions: [
              Container(
                width: 30,
                margin: EdgeInsets.only(right: 10),
                child: IconButton(
                  padding: EdgeInsets.all(5),
                  alignment: Alignment.centerRight,
                  iconSize: 28,
                  icon: Icon(Icons.refresh),
                  onPressed: () => dispatch(AdminDevicesActionCreate.refresh()),
                ),
              ),
            ],
            centerTitle: true,
          ),
          Expanded(
            child: Padding(
              padding: EdgeInsets.all(16),
              child: Column(
                children: [
                  Padding(
                    padding: EdgeInsets.only(top: 5),
                    child: Text('Devices', style: TextStyle(fontSize: 30, fontWeight: FontWeight.w500),),
                  ),
                  Expanded(
                    child: Container(
                      margin: EdgeInsets.only(top: 20),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                        border: Border.all(
                          color: Colors.grey,
                          width: 2
                        )
                      ),
                      child: GridView.builder(
                        gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                          maxCrossAxisExtent: 150
                        ),
                        scrollDirection: Axis.vertical,
                        itemCount: adapter.itemCount,
                        itemBuilder: adapter.itemBuilder
                      )
                    )
                  ),
                  Container(
                    width: 69,
                    height: 69,
                    margin: EdgeInsets.only(top: 20),
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      border: Border.all(
                        color: Colors.grey,
                        width: 2
                      )
                    ),
                    child: FlatButton(
                      shape: CircleBorder(),
                      child: Center(
                        child: Icon(Icons.add, size: 35,)
                      ),
                      onPressed: () => dispatch(AdminDevicesActionCreate.addDevice()),
                    )
                  ),
                ],
              ),
            )
          )
        ]
      )
    )
  );
}