import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/widgets.dart';

import 'page.dart';
import '../shared_states.dart';
import '../mixins.dart';

class AdminDevicesPageState with DatabaseState implements Cloneable<AdminDevicesPageState> {
  List<DeviceState> devices;
  String roomId;

  @override
  AdminDevicesPageState clone() {
    return AdminDevicesPageState()
      ..devices = devices
      ..roomId = roomId;
  }
}

AdminDevicesPageState initState(String roomId) {
  final state = AdminDevicesPageState();
  state.roomId = roomId;
  state.devices = [];
  return state;
}