import 'package:fish_redux/fish_redux.dart';

enum DeviceAction{
  change,
  delete,
  switchEnable,
  refresh
}

class DeviceActionCreate{
  static Action change() => const Action(DeviceAction.change);
  static Action refresh() => const Action(DeviceAction.refresh);
  static Action delete(String id) => Action(DeviceAction.delete, payload: id);
  static Action switchEnable(bool enable) => Action(DeviceAction.switchEnable, payload: enable);
}