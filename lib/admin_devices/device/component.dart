import 'package:fish_redux/fish_redux.dart';

import 'view.dart';
import 'effect.dart';
import '../../device/component.dart';
export 'state.dart';
import 'state.dart';

class DeviceAdminComponent extends Component<DeviceAdminItemState> {
  DeviceAdminComponent()
      : super(
          effect: buildEffect(),
          dependencies: Dependencies(slots: {
            'device': ConnOp(
                    get: (d) => DeviceComponentState()..device = d.device,
                    set: (d, o) {}) +
                DeviceComponent()
          }),
          view: buildView,
        );
}
