import 'package:fish_redux/fish_redux.dart';
import 'package:fish_redux/fish_redux.dart' as fish;
import 'package:flutter/material.dart';

import '../../router.dart';
import 'action.dart';
import 'state.dart';
import 'package:http/http.dart' as http;

Effect<DeviceAdminItemState> buildEffect() {
  return combineEffects({
    DeviceAction.change: _change,
    DeviceAction.switchEnable: _switchEnable,
  });
}

void _change(fish.Action action, Context<DeviceAdminItemState> context) {
  final route = Routes.adminChangeDevice(deviceId: context.state.device.id);
  Navigator.of(context.context).pushNamed(route);
}

Future<void> _switchEnable(
    fish.Action action, Context<DeviceAdminItemState> context) async {
  final bool value = action.payload;
  if (value) {
    await http.get(context.state.device.onUrl);
  } else {
    await http.get(context.state.device.offUrl);
  }
  await context.state.database.devices
      .update(context.state.device.id, {'enabled': action.payload});
  context.dispatch(DeviceActionCreate.refresh());
}
