import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'action.dart';
import 'state.dart';

Widget buildView(
    DeviceAdminItemState state, Dispatch dispatch, ViewService viewService) {
  return Padding(
    padding: EdgeInsets.all(5),
    child: PopupMenuButton(
      onSelected: (action) => dispatch(action),
      itemBuilder: (BuildContext context) => [
        !state.device.enabled
            ? PopupMenuItem(
                value: DeviceActionCreate.switchEnable(true),
                child: Text('Enable'),
              )
            : PopupMenuItem(
                value: DeviceActionCreate.switchEnable(false),
                child: Text('Disable'),
              ),
        PopupMenuItem(
          value: DeviceActionCreate.change(),
          child: Text('Change'),
        ),
        PopupMenuItem(
          value: DeviceActionCreate.delete(state.device.id),
          child: Text('Delete'),
        ),
      ],
      child: IgnorePointer(
        child: viewService.buildComponent('device'),
      ),
    ),
  );
}
