import 'package:fish_redux/fish_redux.dart';

import '../../shared_states.dart';
import '../../mixins.dart';

class DeviceAdminItemState with DatabaseState implements Cloneable<DeviceAdminItemState> {
  DeviceState device;

  @override
  DeviceAdminItemState clone() {
    return DeviceAdminItemState()..device = device;
  }
}