import 'package:fish_redux/fish_redux.dart';

import '../shared_states.dart';
import 'device/component.dart';
import 'device/action.dart';
import 'state.dart';
import 'action.dart';

class DevicesListAdapter extends DynamicFlowAdapter<AdminDevicesPageState>{
  DevicesListAdapter()
      : super(
          pool: <String, Component<Object>>{
            'device': DeviceAdminComponent(),
          },
          effect: (action, context) {
            if(action.type == DeviceAction.refresh) {
              context.dispatch(AdminDevicesActionCreate.refresh());
            }
            if(action.type == DeviceAction.delete) {
              context.dispatch(AdminDevicesActionCreate.delete(action.payload));
            }
          },
          connector: _AdminDevicesListConnector(),
        );
}

class _AdminDevicesListConnector extends ConnOp<AdminDevicesPageState, List<ItemBean>> {
  @override
  List<ItemBean> get(AdminDevicesPageState state) {
    if (state.devices?.isNotEmpty == true) {
      return state.devices
          .map<ItemBean>((data) => ItemBean('device',  DeviceAdminItemState()
            ..device=data
            ..database=state.database)
          )
          .toList(growable: true);
    } else {
      return <ItemBean>[];
    }
  }

  @override
  void set(AdminDevicesPageState state, List<ItemBean> devices) {
    if (devices?.isNotEmpty == true) {
      state.devices = List.from(
          devices.map((bean) => (bean.data as DeviceAdminItemState).device).toList()
      );
    } else {
      state.devices = [];
    }
  }
}