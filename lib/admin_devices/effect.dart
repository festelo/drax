import 'package:fish_redux/fish_redux.dart';
import 'package:fish_redux/fish_redux.dart' as fish;
import 'package:flutter/material.dart';

import '../shared_effects.dart';
import '../router.dart';
import 'state.dart';
import 'action.dart';

Effect<AdminDevicesPageState> buildEffect(){
  return combineEffects({
    AdminDevicesAction.addDevice: _addDevice,
    AdminDevicesAction.refresh: _refresh,
    AdminDevicesAction.delete: _delete,
    Lifecycle.initState: _initState
  });
}

Future<void> _delete(fish.Action action, Context<AdminDevicesPageState> context) async {
  String deviceId = action.payload;
  final roomId = context.state.roomId;
  await context.state.database.rooms.removeDevice(roomId, context.state.database.devices.document(deviceId));
  await context.state.database.devices.delete(roomId);
}

Future<void> _refresh(fish.Action action, Context<AdminDevicesPageState> context) async {
  if(context.state.roomId != null) {
    final roomDoc = await context.state.database.rooms.document(context.state.roomId).get();
    final devices = await context.state.database.devices.getByRoomDocument(roomDoc);
    context.dispatch(AdminDevicesActionCreate.refreshSilent(devices));
  }
}

void _initState(fish.Action action, Context<AdminDevicesPageState> context) {
  final id = context.state.roomId;
  final db =  context.state.database;
  if(id != null) {
    final sub = db.rooms
      .document(id)
      .snapshots()
      .listen((doc) async => 
        context.dispatch(
          AdminDevicesActionCreate.refreshSilent(
            await db.devices.getByRoomDocument(doc)
          )
        )
      );
    context.onDisposed(() async { 
      await sub.cancel(); print('disposed'); 
    });
  }
}

void _addDevice(fish.Action action, Context<AdminDevicesPageState> context){
  final route = Routes.adminCreateDevice(roomId: context.state.roomId);
  Navigator
    .of(context.context)
    .pushNamed(route);
}