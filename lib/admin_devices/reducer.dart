import 'package:fish_redux/fish_redux.dart';

import 'action.dart';
import 'state.dart';

Reducer<AdminDevicesPageState> buildReducer() {
  return asReducer({
    AdminDevicesAction.refreshSilent: _refreshSilent,
  });
}

AdminDevicesPageState _refreshSilent(AdminDevicesPageState state, Action action) {
  return state.clone()
    ..devices = action.payload['devices'];
}