import 'package:fish_redux/fish_redux.dart';
import '../shared_states.dart';

enum AdminDevicesAction{
  addDevice,
  refreshSilent,
  refresh,
  delete
}

class AdminDevicesActionCreate{
  static Action addDevice() => const Action(AdminDevicesAction.addDevice);
  static Action refresh() => const Action(AdminDevicesAction.refresh);
  static Action delete(String deviceId) => Action(AdminDevicesAction.delete, payload: deviceId);
  static Action refreshSilent(List<DeviceState> devices) => Action(AdminDevicesAction.refreshSilent, payload: {'devices': devices});
}