import 'package:fish_redux/fish_redux.dart';

import 'effect.dart';
import 'state.dart';
import 'view.dart';
import 'reducer.dart';
import 'devices_adapter.dart';

class AdminDevicesPage extends Page<AdminDevicesPageState, String> {
  AdminDevicesPage()
      : super(
          initState: initState,
          effect: buildEffect(),
          view: buildView,
          reducer: buildReducer(),
          dependencies: Dependencies(
            adapter: NoneConn() + DevicesListAdapter()
          ),
          middleware: [
            logMiddleware(tag: 'AdminDevicesPage'),
          ],
        );
}