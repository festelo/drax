
import 'package:fluro/fluro.dart';
import './router_handlers.dart';
import 'package:meta/meta.dart';

class Routes {
  static String _loginSource = "/login";
  static String _registerSource = "/register";
  static String _homeSource = "/home";
  static String _roomSource = "/room";
  static String _userSource = "/user";
  static String _adminRoomsSource = "/admin-rooms";
  static String _adminChangeUserSource = "/admin-change-user";
  static String _adminCreateUserSource = "/admin-create-user";
  static String _adminChangeRoomSource = "/admin-change-room";
  static String _adminCreateRoomSource = "/admin-create-room";
  static String _adminChangeDeviceSource = "/admin-change-device";
  static String _adminCreateDeviceSource = "/admin-create-device";
  static String _adminDevicesSource = "/admin-devices";
  static String _adminSource = "/admin";
  static String _adminActionsSource = "/admin-actions";
  static String _loadingSource = "/";

  static String login({String email = '', bool adminLogin = false}) => _loginSource + "?email=$email&adminLogin=$adminLogin";
  static String register({String email = ''}) => _registerSource + "?email=$email";
  static String home({String userId = ''}) => _homeSource + "?userId=$userId";
  static String room({String roomId = ''}) => _roomSource + "?roomId=$roomId";
  static String user({String userId = ''}) => _userSource + "?userId=$userId";
  static String adminRooms({@required String userId}) => _adminRoomsSource + "?userId=$userId";
  static String adminChangeUser({@required String userId}) => _adminChangeUserSource + "?userId=$userId";
  static String adminCreateUser() => _adminCreateUserSource;
  static String adminChangeRoom({@required String roomId}) => _adminChangeRoomSource + "?roomId=$roomId";
  static String adminCreateRoom({@required String userId}) => _adminCreateRoomSource + "?userId=$userId";
  static String adminChangeDevice({@required String deviceId}) => _adminChangeDeviceSource + "?deviceId=$deviceId";
  static String adminCreateDevice({@required String roomId}) => _adminCreateDeviceSource + "?roomId=$roomId";
  static String adminDevices({@required String roomId}) => _adminDevicesSource + "?roomId=$roomId";
  static String adminActions({@required String deviceId}) => _adminActionsSource + "?deviceId=$deviceId";
  static String admin() => _adminSource;
  static String loading() => _loadingSource;

  static void configureRoutes(Router router) {
    router.notFoundHandler = new Handler(
        handlerFunc: (context, params) { 
          print("ROUTE WAS NOT FOUND !!!"); 
        }
    );
    router.define(_loginSource, handler: loginHandler);
    router.define(_registerSource, handler: registerHandler);
    router.define(_homeSource, handler: homeHandler);
    router.define(_roomSource, handler: roomHandler);
    router.define(_userSource, handler: userHandler);
    router.define(_adminSource, handler: adminHandler);
    router.define(_adminRoomsSource, handler: adminRoomsHandler);
    router.define(_adminCreateUserSource, handler: adminCreateUserHandler);
    router.define(_adminChangeUserSource, handler: adminChangeUserHandler);
    router.define(_adminChangeRoomSource, handler: adminChangeRoomHandler);
    router.define(_adminCreateRoomSource, handler: adminCreateRoomHandler);
    router.define(_adminChangeDeviceSource, handler: adminChangeDeviceHandler);
    router.define(_adminCreateDeviceSource, handler: adminCreateDeviceHandler);
    router.define(_adminDevicesSource, handler: adminDevicesHandler);
    router.define(_adminActionsSource, handler: adminActionsHandler);
    router.define(_loadingSource, handler: loadingHandler);
  }
}