import 'dart:io';
import 'package:flutter/services.dart';

import 'firebase/auth.dart';
import 'package:image_picker/image_picker.dart';
import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'router.dart';

void showCustomDialog(BuildContext context, List<ListTile> Function(BuildContext) tiles){
  showModalBottomSheet(context: context,
    builder: (BuildContext buildContext) {
      return new Column(
        mainAxisSize: MainAxisSize.min,
        children: tiles(buildContext)
      );
    }
  );
} 

void saveToClipboard(String text) {
  Clipboard.setData(ClipboardData(text: text));
}

Future<File> showChangePhotoDialog(BuildContext context, {VoidCallback galleryTap, Widget debugWidget}) async {
  return await showModalBottomSheet<File>(context: context,
    builder: (BuildContext buildContext) {
      return new Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          new ListTile(
            leading: new Icon(Icons.camera_alt),
            title: new Text('Take from camera'),
            onTap: () async { 
              final image = await ImagePicker.pickImage(source: ImageSource.camera);
              Navigator.pop(buildContext, image); 
            },          
          ),
          new ListTile(
            leading: new Icon(Icons.photo_album),
            title: new Text('Pick from gallery'),
            onTap: () async { 
              final image = await ImagePicker.pickImage(source: ImageSource.gallery);
              Navigator.pop(buildContext, image); 
            },          
          ),
          debugWidget == null ? Container() : debugWidget
        ],
      );
    }
  );
} 

void showSnackMessage(ScaffoldState scaffold, String message) {
  scaffold.showSnackBar(
    SnackBar(
      content: Text(message),
    )
  );
}

void logInSilent(Auth auth, NavigatorState navigator){
  final route = Routes.home(userId: auth.user?.uid);
  navigator.pushNamedAndRemoveUntil(route, (a) => false);
}

void logInAdminSilent(NavigatorState navigator){
  final route = Routes.admin();
  navigator.pushNamedAndRemoveUntil(route, (a) => false);
}

void logout(Auth auth){
  auth.signOut();
}

void goBack(BuildContext context){
  Navigator
    .of(context)
    .pop();
}