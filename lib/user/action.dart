import 'package:fish_redux/fish_redux.dart';
import '../shared_states.dart';
import 'element_key.dart';

enum UserAction{
  setLoading,
  refreshSilent,
  refresh,
  changePassword,
  changePhoto,
  saveId,
  goBack
}

class UserActionCreate{
  static Action refreshSilent(UserState user) => Action(UserAction.refreshSilent, payload: { 'user' : user});
  static Action setLoading(bool loading, {ElementKey element}) => Action(
    UserAction.setLoading, 
    payload: {
      'value': loading,
      'element': element
    }
  );
  static Action refresh() => const Action(UserAction.refresh);
  static Action changePassword() => const Action(UserAction.changePassword);
  static Action changePhoto() => const Action(UserAction.changePhoto);
  static Action saveId() => const Action(UserAction.saveId);
  static Action goBack() => const Action(UserAction.goBack);
}