import 'package:fish_redux/fish_redux.dart';
import 'package:fish_redux/fish_redux.dart' as fish;
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../shared_effects.dart';
import '../router.dart';
import 'state.dart';
import 'action.dart';
import 'element_key.dart';

Effect<UserPageState> buildEffect(){
  return combineEffects({
    UserAction.saveId: (a, c) => saveToClipboard(c.state.user.id),
    UserAction.changePassword: _changePassword,
    UserAction.changePhoto: _changePhoto,
    UserAction.refresh: _refresh,
    UserAction.goBack: _goBack,
    Lifecycle.initState: _initState
  });
}

Future<void> _refresh(fish.Action action, Context<UserPageState> context) async {
  if(context.state.user.id != null) {
    context.dispatch(UserActionCreate.setLoading(true));
    final user = await context.state.database.users.get(context.state.user.id);
    context.dispatch(UserActionCreate.refreshSilent(user));
    context.dispatch(UserActionCreate.setLoading(false));
  }
}

Future<void> _initState(fish.Action action, Context<UserPageState> context) async {
  context.dispatch(UserActionCreate.refresh());
}

void _goBack(fish.Action action, Context<UserPageState> context){
  goBack(context.context);
}

Future<void> _changePhoto(fish.Action action, Context<UserPageState> context) async {
  final db = context.state.database;
  final storage = context.state.storage;
  final user = context.state.user;

  if(user.id == null) {
    showSnackMessage(context.state.scaffoldKey.currentState, 'You should to set info first');
    return;
  }
  else {
    final file = await showChangePhotoDialog(context.context);
    if(file == null) return;
    context.dispatch(UserActionCreate.setLoading(true, element: ElementKey.changePhoto));
    try{
      final url = await storage.upload(file, 'user_${user.id}');
      await db.users.update(user.id, {
        'photoUrl': url,
        'localImage': false
      });
    }
    catch (e) {
      showSnackMessage(context.state.scaffoldKey.currentState, e.toString());
    }
    finally {
      context.dispatch(UserActionCreate.setLoading(false, element: ElementKey.changePhoto));
      context.dispatch(UserActionCreate.refresh());
    }
  }
}

Future<void> _changePassword(fish.Action action, Context<UserPageState> context) async {
  context.dispatch(UserActionCreate.setLoading(true, element: ElementKey.passwordSave));
  try {
    await context.state.auth.user.updatePassword(context.state.passwordEditController.text);
  }
  on PlatformException catch (e) {
    showSnackMessage(context.state.scaffoldKey.currentState, e.message);
  }
  catch (e) {
    showSnackMessage(context.state.scaffoldKey.currentState, e.toString());
  }
  finally {
    context.dispatch(UserActionCreate.setLoading(false, element: ElementKey.passwordSave));
  }
}