import 'package:fish_redux/fish_redux.dart';
import 'package:fish_redux/fish_redux.dart' as fish;
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

import '../components.dart';
import 'state.dart';
import 'action.dart';
import 'element_key.dart';


Widget buildView(UserPageState state, Dispatch dispatch, ViewService viewService) {
  Function _if(ElementKey key, fish.Action action) { return !state.buttonsLoading[key] ? () => dispatch(action) : null; }

  return Scaffold(
    key: state.scaffoldKey,
    body: Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          fit: BoxFit.cover,
          alignment: Alignment.topLeft,
          colorFilter: new ColorFilter.mode(Color(0xFF373447), BlendMode.dstATop),
          image: AssetImage('assets/backs/login.png')
        )
      ),
      padding: const EdgeInsets.symmetric(horizontal: 0.0),
      child: CustomScrollView( 
        slivers: [
          SliverAppBar(
            centerTitle: true,
            backgroundColor: Colors.transparent,
            title: Text('Admin panel', style: TextStyle(fontSize: 35, fontWeight: FontWeight.w500),),
          ),
          state.loading 
            ? SliverFillRemaining(child: Center(child: CircularProgressIndicator()))
            : SliverPadding(
              padding: EdgeInsets.symmetric(horizontal: 16),
              sliver: SliverList(
                delegate: SliverChildListDelegate(
                  [
                    Container(
                      padding: EdgeInsets.only(top: 30),
                      child: Center(
                        child: CircleAvatar(
                          radius: 75,
                          backgroundImage: state.user.photoUrl == null ? AssetImage('assets/demo/user-photo.png') : NetworkImage(state.user.photoUrl),
                        ),
                      ),
                    ),
                    
                    Padding(
                      padding: EdgeInsets.only(top: 10),
                      child: Text(
                        state.user?.name, 
                        style: TextStyle(
                          fontSize: 26, 
                          color: Colors.white70,
                          fontWeight: FontWeight.w500
                        ), 
                        textAlign: TextAlign.center,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 40),
                      child: Text('Password', style: TextStyle(fontSize: 24, fontWeight: FontWeight.w500), textAlign: TextAlign.center),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 20),
                      padding: EdgeInsets.only(top: 0),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                        border: Border.all(
                          color: Colors.grey,
                          width: 2
                        )
                      ),
                      child: Column(
                        children: [
                          DraxTextField(
                            leftIcon: Icon(Icons.lock_outline), 
                            controller: state.passwordEditController,
                            obscureText: true,
                            hintText: 'Password',
                            padding: EdgeInsets.only(top: 10),
                            scrollPadding: EdgeInsets.all(80),
                          ),
                          RectButton(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.vertical(bottom: Radius.circular(10)),
                              border: Border.all(width: 1)
                            ),
                            margin: EdgeInsets.only(top: 1),
                            text: 'Save',
                            onPressed: _if(ElementKey.passwordSave, UserActionCreate.changePassword()),
                          )
                        ],
                      ),
                    ),
                    RectButton(
                      decoration: BoxDecoration(
                        border: Border.all(color: Colors.grey, width: 2)
                      ),
                      margin: EdgeInsets.only(top: 40),
                      text: 'Change photo',
                      onPressed: _if(ElementKey.changePhoto, UserActionCreate.changePhoto()),
                    )
                  ]
                ),
              ) 
            )
          ]
        )
    )
  );
}