import 'package:fish_redux/fish_redux.dart';
import 'package:fish_redux/fish_redux.dart' as fish;
import 'package:flutter/widgets.dart';

import '../shared_states.dart';
import 'action.dart';
import 'state.dart';
import 'element_key.dart';

Reducer<UserPageState> buildReducer() {
  return asReducer({
    UserAction.refreshSilent: _refreshSilent,
    UserAction.setLoading: _setLoading,
  });
}

UserPageState _refreshSilent(UserPageState state, fish.Action action) {
  UserState user = action.payload['user'];
  return state.clone()
    ..user = user
    ..loading = false;
}

UserPageState _setLoading(UserPageState state, fish.Action action) {
  final ElementKey element = action.payload['element'];
  final bool value = action.payload['value'];

  if(element == null)
    return state.clone()
      ..loading = value;
  else
    return state.clone()
      ..buttonsLoading[element] = value;
}