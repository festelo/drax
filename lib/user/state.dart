import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'page.dart';
import '../shared_states.dart';
import '../mixins.dart';
import 'element_key.dart';

class UserPageState with DatabaseState, AuthState, StorageState, FunctionsState implements Cloneable<UserPageState> {
  GlobalKey<State<Scaffold>> scaffoldKey;
  TextEditingController passwordEditController;
  UserState user;
  bool loading;
  Map<ElementKey, bool> buttonsLoading = Map.fromIterable(ElementKey.values, key: (v) => v, value: (v) => false);

  @override
  UserPageState clone() {
    return UserPageState()
      ..loading = loading
      ..buttonsLoading = buttonsLoading
      ..scaffoldKey = scaffoldKey
      ..passwordEditController = passwordEditController
      ..user = user;
  }
}

UserPageState initState(String userId) {
  final state = UserPageState();
  state.scaffoldKey = GlobalKey();
  state.passwordEditController = TextEditingController();
  state.loading = false;
  state.user = UserState()
    ..id = userId;
  return state;
}