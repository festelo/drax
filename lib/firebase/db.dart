import 'package:cloud_firestore/cloud_firestore.dart';
import '../shared_states.dart';

class _DatabaseCollection<T extends DatabaseMappedState> {
  final String collectionName;
  final Firestore store;
  final T Function(String id, Map<String, dynamic> data) constructor;
  _DatabaseCollection(this.store, this.constructor, this.collectionName);

  Future<T> get(String id) async {
    final doc = await store.collection(collectionName).document(id).get();
    return constructor(doc.documentID, doc.data);
  }

  T fromDocument(DocumentSnapshot doc) {
    return constructor(doc.documentID, doc.data);
  }

  Future<List<T>> getAll() async {
    final docs = await store.collection(collectionName).getDocuments();
    return docs.documents.map((d) => constructor(d.documentID, d.data)).toList();
  }
  
  Stream<QuerySnapshot> snapshots(){
    return store.collection(collectionName).snapshots();
  }

  DocumentReference document(String id){
    return store.collection(collectionName).document(id);
  }

  Future<void> update(String id, Map<String, dynamic> data) async {
    await document(id).updateData(data);
  }

  Future<bool> exist(String id) async {
    final doc = await document(id).get();
    return doc.exists;
  }

  Future<String> create(T state, {Map<String, dynamic> additional}) async {
    final map = state.toMap();
    if(additional != null) { map.addAll(additional); }
    final doc = await store.collection(collectionName).add(map);
    return doc.documentID;
  }
  Future<void> createWithId(T state, String id) async {
    await document(state.id).setData(state.toMap());
  }
  Future<void> delete(String id) async {
    await document(id).delete();
  }
}

class _RoomsCollection extends _DatabaseCollection<RoomState> {
  final Firestore store;
  _RoomsCollection(this.store) : super(store, (id, data) => RoomState.fromMap(id, data), 'rooms');

  Future<List<RoomState>> getByUserDocument(DocumentSnapshot doc) async {
    final rooms = doc.data['rooms'];
    if(rooms == null) return [];
    final roomRefs = (rooms as List<dynamic>).cast<DocumentReference>();
    final roomDocs = await Future.wait(roomRefs.map((r) => r.get()));
    return roomDocs.map((r) => RoomState.fromMap(r.documentID, r.data)).toList();
  }

  Future<void> _changeDevices(String roomId, List<DocumentReference> Function(List<DocumentReference>) changer) async {
    final doc = await document(roomId).get();
    final devices = doc.data['devices'] ?? [];
    final deviceRefs = (devices as List<dynamic>).cast<DocumentReference>();
    final newList = changer(List.from(deviceRefs));
    await update(roomId, {'devices': newList});
  }
  
  Future<void> addDevice(String roomId, DocumentReference device) async {
    await _changeDevices(roomId, (l) => l..add(device));
  }

  Future<void> removeDevice(String roomId, DocumentReference device) async {
    await _changeDevices(roomId, (l) => l..remove(device));
  }
}

class _UsersCollection extends _DatabaseCollection<UserState> {
  _UsersCollection(Firestore store) : super(store, (id, data) => UserState.fromMap(id, data), 'users');

  Future<bool> isAdmin(String userId) async {
    final doc = await store.collection('users').document(userId).get();
    return doc.data['isAdmin'] == true;
  }

  Future<void> _changeRoom(String userId, List<DocumentReference> Function(List<DocumentReference>) changer) async {
    final doc = await document(userId).get();
    final devices = doc.data['rooms'] ?? [];
    final deviceRefs = (devices as List<dynamic>).cast<DocumentReference>();
    final newList = changer(List.from(deviceRefs));
    await update(userId, {'rooms': newList});
  }
  
  Future<void> addRoom(String userId, DocumentReference room) async {
    await _changeRoom(userId, (l) => l..add(room));
  }

  Future<void> removeRoom(String userId, DocumentReference room) async {
    await _changeRoom(userId, (l) => l..remove(room));
  }
}

class _DevicesCollection extends _DatabaseCollection<DeviceState> {
  _DevicesCollection(Firestore store) : super(store, (id, data) => DeviceState.fromMap(id, data), 'devices');


  Future<List<DeviceState>> getByRoomDocument(DocumentSnapshot doc) async {
    final deviceRefs = getDocumentsByRoomDocument(doc);
    final deviceDocs = await Future.wait(deviceRefs.map((r) => r.get()));
    return deviceDocs.map((r) => DeviceState.fromMap(r.documentID, r.data)).toList();
  }

  List<DocumentReference> getDocumentsByRoomDocument(DocumentSnapshot doc)  {
    final devices = doc.data['devices'];
    if(devices == null) return [];
    return (devices as List<dynamic>).cast<DocumentReference>();
  }

  Future<DocumentReference> getOwnerDocument(String deviceId) async {
    final doc = await document(deviceId).get();
    return doc.data['owner'];
  }
}

class Database {
  final Firestore store;
  static final Database instance = Database(Firestore.instance);

  _UsersCollection users;
  _RoomsCollection rooms;
  _DevicesCollection devices;

  Database(this.store) : 
    users = _UsersCollection(store),
    rooms = _RoomsCollection(store),
    devices = _DevicesCollection(store);
}