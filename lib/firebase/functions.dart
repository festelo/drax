import 'dart:async';
import 'package:cloud_functions/cloud_functions.dart';

class Functions {
  static final Functions instance = Functions(
    functions: CloudFunctions.instance
  );
  final CloudFunctions functions;

  Functions({this.functions});
  
  Future<dynamic> call(String name, Map<String, dynamic> params) async {
    final result = await functions.getHttpsCallable(functionName: name, parameters: params).call(params);
    return result.data;
  }
}

