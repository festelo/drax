import 'dart:async';
import 'dart:io';

import 'package:firebase_storage/firebase_storage.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_auth/firebase_auth.dart';

class Storage {
  static final Storage instance = Storage(
    uidFetcher: () async => (await FirebaseAuth.instance.currentUser()).uid,
    storage: FirebaseStorage(
      app: FirebaseApp.instance, 
      storageBucket: 'gs://drax-307b8.appspot.com/'
    )
  );
  final FirebaseStorage storage;
  final FutureOr<String> Function() uidFetcher;

  Storage({this.storage, this.uidFetcher});
  
  Future<String> upload(File file, String name) async {
    final uid = await uidFetcher();
    final ref = storage.ref().child(uid).child(name);
    final snapshot = await ref.putFile(file, StorageMetadata(contentType: 'image/jpg')).onComplete;
    return await ref.getDownloadURL();
  }
}

