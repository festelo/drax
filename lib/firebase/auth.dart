import 'dart:ui';
import 'package:firebase_auth/firebase_auth.dart';

class Auth {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  static final Auth instance = Auth();

  FirebaseUser get user => _user;
  FirebaseUser _user;

  void init({VoidCallback loginedOut, VoidCallback loginedIn}){
    _auth.onAuthStateChanged.listen((user) => _authStateChanged(user, loginedOut, loginedIn));
  }

  void _authStateChanged(FirebaseUser user, VoidCallback loginedOut, VoidCallback loginedIn) {
    _user = user;
    if(user == null) {
      print('authState changed: user exited');
      if(loginedOut != null) { loginedOut(); }
    }
    else {
      print('authState changed: user entered');
      if(loginedIn != null) { loginedIn(); }
    }
  }

  Future<FirebaseUser> getFreshUser() async {
    final FirebaseUser user = await _auth.currentUser();
    return user;
  }

  Future<FirebaseUser> signUp(String email, String pass) async {
    final FirebaseUser user = await _auth.createUserWithEmailAndPassword(email: email, password: pass);
    return user;
  }

  Future<FirebaseUser> signIn(String email, String pass) async {
    final FirebaseUser user = await _auth.signInWithEmailAndPassword(email: email, password: pass);
    print("signed in " + (user.displayName?.toString() ?? 'undef') + " " + user.uid);
    return user;
  }

  Future<void> signOut() async {
    await _auth.signOut();
    print("signed out");
  }
}

