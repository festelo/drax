import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

import '../components.dart';
import 'state.dart';
import 'action.dart';

Widget buildView(AdminPageState state, Dispatch dispatch, ViewService viewService) {
  final ListAdapter adapter = viewService.buildAdapter();
  return Scaffold(
    body: Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          fit: BoxFit.cover,
          alignment: Alignment.topLeft,
          colorFilter: new ColorFilter.mode(Color(0xFF373447), BlendMode.dstATop),
          image: AssetImage('assets/backs/login.png')
        )
      ),
      padding: const EdgeInsets.all(16.0),
      child: Column(
        children: [
          Padding(
            padding: EdgeInsets.only(top: 45),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  width: 30,
                ),
                Text('Admin panel', style: TextStyle(fontSize: 35, fontWeight: FontWeight.w500),),
                Container(
                  width: 30,
                  child: IconButton(
                    padding: EdgeInsets.all(5),
                    alignment: Alignment.centerRight,
                    iconSize: 28,
                    icon: Icon(Icons.refresh),
                    onPressed: () => dispatch(AdminActionCreate.refresh()),
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 25),
            child: Text('Users', style: TextStyle(fontSize: 30, fontWeight: FontWeight.w500),),
          ),
          Expanded(
            child: Container(
              margin: EdgeInsets.only(top: 20),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(10)),
                border: Border.all(
                  color: Colors.grey,
                  width: 2
                )
              ),
              child: state.loading 
                ? Center(child: CircularProgressIndicator()) 
                : GridView.builder(
                    gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                      maxCrossAxisExtent: 100
                    ),
                    scrollDirection: Axis.vertical,
                    itemCount: adapter.itemCount,
                    itemBuilder: adapter.itemBuilder
                  )
            )
          ),
          Container(
            width: 69,
            height: 69,
            margin: EdgeInsets.only(top: 20),
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              border: Border.all(
                color: Colors.grey,
                width: 2
              )
            ),
            child: FlatButton(
              shape: CircleBorder(),
              child: Center(
                child: Icon(Icons.add, size: 35),
              ),
              onPressed: () => dispatch(AdminActionCreate.addUser()),
            )
          ),
          Padding(
            padding: EdgeInsets.only(top: 20, bottom: 5),
            child: FlatButton(
              child: Text('Log out', style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),),
              onPressed: () => dispatch(AdminActionCreate.logout()),
            ),
          )
        ]
      )
    )
  );
}