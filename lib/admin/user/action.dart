import 'package:fish_redux/fish_redux.dart';

enum UserAction{
  openRooms,
  change,
  delete,
}

class UserActionCreate{
  static Action openRooms() => const Action(UserAction.openRooms);
  static Action change() => const Action(UserAction.change);
  static Action delete(String userId) => Action(UserAction.delete, payload: userId);
}