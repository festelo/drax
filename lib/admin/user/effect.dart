import 'package:fish_redux/fish_redux.dart';
import 'package:fish_redux/fish_redux.dart' as fish;
import 'package:flutter/material.dart';

import '../../router.dart';
import '../../shared_states.dart';
import 'action.dart';

Effect<UserState> buildEffect(){
  return combineEffects({
    UserAction.change: _change,
    UserAction.openRooms: _openRooms,
  });
}

void _openRooms(fish.Action action, Context<UserState> context){
  final route = Routes.adminRooms(userId: context.state.id);
  Navigator
    .of(context.context)
    .pushNamed(route);
}

void _change(fish.Action action, Context<UserState> context){
  final route = Routes.adminChangeUser(userId: context.state.id);
  Navigator
    .of(context.context)
    .pushNamed(route);
}