import 'package:fish_redux/fish_redux.dart';

import '../../shared_states.dart';
import 'view.dart';
import 'effect.dart';

class UserComponent extends Component<UserState> {
  UserComponent()
      : super(
          effect: buildEffect(),
          view: buildView,
        );
}