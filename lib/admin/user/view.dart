import 'dart:ui';

import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

import '../../components.dart';
import '../../shared_states.dart';
import 'action.dart';

Widget buildView(UserState state, Dispatch dispatch, ViewService viewService) {
  return Padding( 
    padding: EdgeInsets.all(5),
    child: PopupMenuButton(
      onSelected: (action) => dispatch(action),
      itemBuilder: (BuildContext context) => [
        PopupMenuItem(
          value: UserActionCreate.openRooms(),
          child: Text('Rooms'),
        ),
        PopupMenuItem(
          value: UserActionCreate.change(),
          child: Text('Change'),
        ),
        PopupMenuItem(
          value: UserActionCreate.delete(state.id),
          child: Text('Delete'),
        ),
      ],
      child: state.photoUrl == null 
        ? SignedImage(
          image: AssetImage('assets/demo/user-photo.png'),
          text: state.name,
        )
        : SignedImage(
          image: NetworkImage(state.photoUrl),
          text: state.name,
        )
    ),
  );
}