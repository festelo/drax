import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/widgets.dart';

import 'page.dart';
import '../shared_states.dart';
import '../mixins.dart';

class AdminPageState with FunctionsState, AuthState, DatabaseState implements Cloneable<AdminPageState> {
  List<UserState> users;
  bool loading;

  @override
  AdminPageState clone() {
    return AdminPageState() 
      ..loading = loading
      ..users = users;
  }
}

AdminPageState initState(dynamic params) {
  final state = AdminPageState();
  state.loading = true;
  return state;
}