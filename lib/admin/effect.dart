import 'package:fish_redux/fish_redux.dart' as fish;
import 'package:flutter/material.dart';

import '../shared_effects.dart';
import '../router.dart';
import 'state.dart';
import 'action.dart';

fish.Effect<AdminPageState> buildEffect(){
  return fish.combineEffects({
    AdminAction.addUser: _addUser,
    AdminAction.logout: _logout,
    AdminAction.refresh: _refresh,
    AdminAction.delete: _delete,
    fish.Lifecycle.initState: _initState
  });
}

Future<void> _initState(fish.Action action, fish.Context<AdminPageState> context) async {
  context.dispatch(AdminActionCreate.refresh());
}
Future<void> _refresh(fish.Action action, fish.Context<AdminPageState> context) async {
  context.dispatch(AdminActionCreate.setLoading(true));
  final users = await context.state.database.users.getAll();
  context.dispatch(AdminActionCreate.refreshSilent(users));
  context.dispatch(AdminActionCreate.setLoading(false));
}
void _addUser(fish.Action action, fish.Context<AdminPageState> context){
  final route = Routes.adminCreateUser();
  Navigator
    .of(context.context)
    .pushNamed(route);
}
void _logout(fish.Action action, fish.Context<AdminPageState> context){
  logout(context.state.auth);
}
Future<void> _delete(fish.Action action, fish.Context<AdminPageState> context) async {
  String userId = action.payload;
  context.dispatch(AdminActionCreate.setLoading(true));
  await context.state.functions.call('deleteAccount', {
    'userId': userId
  });
  await context.state.database.users.delete(userId);
  context.dispatch(AdminActionCreate.refresh());
}