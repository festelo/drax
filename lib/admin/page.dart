import 'package:fish_redux/fish_redux.dart';

import 'effect.dart';
import 'state.dart';
import 'view.dart';
import 'reducer.dart';
import 'users_adapter.dart';

class AdminPage extends Page<AdminPageState, void> {
  AdminPage()
      : super(
          initState: initState,
          effect: buildEffect(),
          reducer: buildReducer(),
          view: buildView,
          dependencies: Dependencies(
            adapter: NoneConn() + UsersListAdapter()
          ),
          middleware: [
            logMiddleware(tag: 'AdminPage'),
          ],
        );
}