import 'package:fish_redux/fish_redux.dart';
import '../shared_states.dart';

enum AdminAction{
  refreshSilent,
  addUser,
  logout,
  setLoading,
  delete,
  refresh
}

class AdminActionCreate{
  static Action refreshSilent(List<UserState> users) => Action(AdminAction.refreshSilent, payload: { 'users' : users});
  static Action setLoading(bool val) => Action(AdminAction.setLoading, payload: val);
  static Action addUser() => const Action(AdminAction.addUser);
  static Action refresh() => const Action(AdminAction.refresh);
  static Action logout() => const Action(AdminAction.logout);
  static Action delete(String userId) => Action(AdminAction.delete, payload: userId);
}