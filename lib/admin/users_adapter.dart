import 'package:fish_redux/fish_redux.dart';

import '../shared_states.dart';
import 'user/component.dart';
import 'user/action.dart';
import 'state.dart';
import 'action.dart';

class UsersListAdapter extends DynamicFlowAdapter<AdminPageState>{
  UsersListAdapter()
      : super(
          pool: <String, Component<Object>>{
            'user': UserComponent(),
          },
          effect: (action, context) {
            if(action.type == UserAction.delete) {
              context.dispatch(AdminActionCreate.delete(action.payload));
            }
          },
          connector: _UsersListConnector(),
        );
}

class _UsersListConnector extends ConnOp<AdminPageState, List<ItemBean>> {
  @override
  List<ItemBean> get(AdminPageState state) {
    if (state.users?.isNotEmpty == true) {
      return state.users
          .map<ItemBean>((data) => ItemBean('user', data))
          .toList(growable: true);
    } else {
      return <ItemBean>[];
    }
  }

  @override
  void set(AdminPageState state, List<ItemBean> devices) {
    if (devices?.isNotEmpty == true) {
      state.users = List.from(
          devices.map((bean) => bean.data).toList()
      );
    } else {
      state.users = [];
    }
  }
}