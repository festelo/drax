import 'package:fish_redux/fish_redux.dart';

import 'action.dart';
import 'state.dart';

Reducer<AdminPageState> buildReducer() {
  return asReducer({
    AdminAction.refreshSilent: _refreshSilent,
    AdminAction.setLoading: _setLoading,
  });
}

AdminPageState _setLoading(AdminPageState state, Action action) {
  return state.clone()
    ..loading = action.payload;
}

AdminPageState _refreshSilent(AdminPageState state, Action action) {
  return state.clone()
    ..users = action.payload['users']
    ..loading = false;
}