import 'package:fish_redux/fish_redux.dart';

import 'action.dart';
import 'state.dart';

Reducer<ActionsState> buildReducer() {
  return asReducer({
    Actions.updateSingleSilent: _updateSingleSilent,
    Actions.refreshSilent: _refreshSilent,
    Actions.setLoading: _setLoading,
    Actions.delete: _delete,
  });
}

ActionsState _setLoading(ActionsState state, Action action) {
  return state.clone()..loading = action.payload;
}

ActionsState _refreshSilent(ActionsState state, Action action) {
  return state.clone()..actions = action.payload;
}

ActionsState _updateSingleSilent(ActionsState state, Action action) {
  final String key = action.payload['key'];
  final String name = action.payload['name'];
  final String url = action.payload['url'];
  return state.clone()
    ..actions = (Map.from(state.actions ?? {})
      ..remove(key)
      ..addAll({name: url}));
}

ActionsState _delete(ActionsState state, Action action) {
  final String key = action.payload;
  return state.clone()..actions = (Map.from(state.actions)..remove(key));
}
