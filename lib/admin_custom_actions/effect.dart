import 'package:fish_redux/fish_redux.dart';
import 'package:fish_redux/fish_redux.dart' as fish;
import 'package:flutter/material.dart';

import '../shared_effects.dart';
import 'state.dart';
import 'action.dart' as action;
import 'action.dart';
import 'action_dialog.dart';

Effect<ActionsState> buildEffect() {
  final effects = {
    action.Actions.goBack: (a, ctx) => goBack(ctx.context),
    action.Actions.addAction: _addAction,
    action.Actions.refresh: _refresh,
    action.Actions.save: _save,
    action.Actions.updateSingle: _updateSingle,
    Lifecycle.initState: _initState,
  };
  return combineEffects(effects);
}

Future<void> _save(
    fish.Action action, fish.Context<ActionsState> context) async {
  final deviceId = context.state.deviceId;
  if (deviceId == null) throw Exception('deviceId is null');
  context.dispatch(ActionCreate.setLoading(true));
  await context.state.database.devices
      .update(deviceId, {'customActions': context.state.actions});
  context.dispatch(ActionCreate.refresh());
}

Future<void> _refresh(
    fish.Action action, fish.Context<ActionsState> context) async {
  if (context.state.deviceId != null) {
    final device =
        await context.state.database.devices.get(context.state.deviceId);
    context.dispatch(ActionCreate.setLoading(true));
    context.dispatch(ActionCreate.refreshSilent(device.customActions));
    context.dispatch(ActionCreate.setLoading(false));
  }
}

void _initState(fish.Action action, fish.Context<ActionsState> context) {
  context.dispatch(ActionCreate.refresh());
}

void _addAction(fish.Action action, fish.Context<ActionsState> context) {
  showDialog(
    context: context.state.scaffoldKey.currentContext,
    builder: (ctx) => ActionDialog(
      dialogContext: ctx,
      dispatch: context.dispatch,
      deletable: false,
    ),
  );
}

void _updateSingle(fish.Action action, fish.Context<ActionsState> context) {
  final String key = action.payload;
  final url = context.state.actions[key];
  showDialog(
    context: context.state.scaffoldKey.currentContext,
    builder: (ctx) => ActionDialog(
      dialogContext: ctx,
      dispatch: context.dispatch,
      deletable: true,
      name: key,
      url: url,
    ),
  );
}
