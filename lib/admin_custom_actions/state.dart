import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import '../mixins.dart';

class ActionsState with DatabaseState implements Cloneable<ActionsState> {
  GlobalKey<State<Scaffold>> scaffoldKey;
  Map<String, String> actions;
  String deviceId;
  bool loading;

  @override
  ActionsState clone() {
    return ActionsState()
      ..loading = loading
      ..actions = actions
      ..deviceId = deviceId
      ..scaffoldKey = scaffoldKey;
  }
}

ActionsState initState(String deviceId) {
  final state = ActionsState();
  state.loading = false;
  state.deviceId = deviceId;
  state.scaffoldKey = GlobalKey();
  return state;
}
