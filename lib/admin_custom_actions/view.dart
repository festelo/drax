import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';

import '../components.dart';
import 'state.dart';
import 'action.dart';

Widget singleAction(String name, Dispatch dispatch) {
  return Container(
    decoration: BoxDecoration(
      border: Border.all(color: Colors.grey, width: 1),
    ),
    margin: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
    padding: EdgeInsets.all(0),
    child: FlatButton(
      padding: EdgeInsets.all(0),
      child: Text(
        name,
        style: TextStyle(fontWeight: FontWeight.w600),
      ),
      onPressed: () => dispatch(
        ActionCreate.updateSingle(name),
      ),
    ),
  );
}

Widget buildView(
    ActionsState state, Dispatch dispatch, ViewService viewService) {
  return Scaffold(
    key: state.scaffoldKey,
    floatingActionButton: Padding(
      padding: EdgeInsets.only(bottom: 80),
      child: FloatingActionButton(
        backgroundColor: Colors.white,
        child: Text(
          '+',
          style: TextStyle(fontSize: 50, color: Colors.black),
        ),
        onPressed: () => dispatch(
          ActionCreate.addAction(),
        ),
      ),
    ),
    body: Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          fit: BoxFit.cover,
          alignment: Alignment.topLeft,
          colorFilter: new ColorFilter.mode(
            Color(0xFF373447),
            BlendMode.dstATop,
          ),
          image: AssetImage('assets/backs/login.png'),
        ),
      ),
      padding: const EdgeInsets.only(top: 16.0),
      child: Column(
        children: [
          Expanded(
            child: CustomScrollView(
              slivers: <Widget>[
                SliverAppBar(
                  backgroundColor: Colors.transparent,
                  title: Text(
                    'Actions',
                    style: TextStyle(fontSize: 35, fontWeight: FontWeight.w500),
                  ),
                ),
                SliverPadding(
                  padding: EdgeInsets.only(top: 20),
                  sliver: SliverList(
                    delegate: SliverChildListDelegate(state.actions?.keys
                            ?.map((name) => singleAction(name, dispatch))
                            ?.toList() ??
                        []),
                  ),
                ),
              ],
            ),
          ),
          RectButton(
            onPressed:
                state.loading ? null : () => dispatch(ActionCreate.save()),
            text: 'Save',
            margin: EdgeInsets.only(bottom: 20, left: 20, right: 20, top: 10),
          )
        ],
      ),
    ),
  );
}
