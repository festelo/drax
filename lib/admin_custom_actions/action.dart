import 'package:fish_redux/fish_redux.dart';

enum Actions {
  addAction,
  goBack,
  refreshSilent,
  refresh,
  save,
  setLoading,

  updateSingle,
  updateSingleSilent,
  delete,
}

class ActionCreate {
  static Action addAction() => const Action(Actions.addAction);
  static Action goBack() => const Action(Actions.goBack);
  static Action refresh() => const Action(Actions.refresh);
  static Action save() => const Action(Actions.save);
  static Action setLoading(bool val) =>
      Action(Actions.setLoading, payload: val);
  static Action refreshSilent(Map<String, String> actions) =>
      Action(Actions.refreshSilent, payload: actions);

  static Action updateSingle(String name) =>
      Action(Actions.updateSingle, payload: name);
  static Action updateSingleSilent(String oldname, String name, String url) =>
      Action(Actions.updateSingleSilent,
          payload: {'key': oldname, 'name': name, 'url': url});
  static Action delete(String name) => Action(Actions.delete, payload: name);
}
