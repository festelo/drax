import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import '../components.dart';
import 'action.dart';


class ActionDialog extends StatefulWidget {
  ActionDialog({
    @required this.dialogContext, 
    @required this.dispatch,
    this.name = '', 
    this.url = '', 
    this.deletable = true
  });

  final BuildContext dialogContext;
  final Dispatch dispatch;
  final String name;
  final String url;
  final bool deletable;

  @override
  State<StatefulWidget> createState() => _ActionDialog();
}
class _ActionDialog extends State<ActionDialog> {
  TextEditingController nameController;
  TextEditingController urlController;

  @override
  void initState() {
    super.initState();
    nameController = TextEditingController(text: widget.name);
    urlController = TextEditingController(text: widget.url);
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      backgroundColor: Colors.black.withOpacity(0.6),
      contentTextStyle: TextStyle(
        fontSize: 15,
        fontStyle: FontStyle.normal
      ),
      titleTextStyle: TextStyle(
        fontSize: 22,
        fontStyle: FontStyle.normal,
        fontWeight: FontWeight.w600
      ),
      title: Center(
        child: Text('Action')
      ),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(15)),
        side: BorderSide(
          color: Colors.white,
          width: 0.2
        )
      ),
      titlePadding: EdgeInsets.only(bottom: 25, top: 35),
      contentPadding: EdgeInsets.symmetric(vertical: 10, horizontal: 24),
      content: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            DraxTextField(
              padding: EdgeInsets.symmetric(vertical: 5),
              hintText: 'Name',
              controller: nameController,
            ),
            DraxTextField(
              padding: EdgeInsets.symmetric(vertical: 5),
              hintText: 'Url',
              controller: urlController,
            ),
            Padding(
              padding: EdgeInsets.only(top: 30, bottom: 5),
              child: Column(
                children: [
                  RectButton(
                    text: 'Save',
                    margin: EdgeInsets.only(top: 5),
                    onPressed: () {
                      widget.dispatch(ActionCreate.updateSingleSilent(widget.name, nameController.text, urlController.text));
                      Navigator.of(widget.dialogContext).pop();
                    }
                  ),
                  Container(
                    height: 40,
                    child: FlatButton(
                      padding: EdgeInsets.only(top: 5),
                      child: Text(
                        'Delete',
                        style: TextStyle(
                          fontSize: 12,
                          fontWeight: FontWeight.w400,
                          color: Colors.grey
                        ),
                      ),
                      onPressed: widget.deletable 
                        ? () {
                          widget.dispatch(ActionCreate.delete(widget.name));
                          Navigator.of(widget.dialogContext).pop();
                        }
                        : null
                    ),
                  )
                ],
              )
            ),
          ],
        ),
      )
    );
  }
}