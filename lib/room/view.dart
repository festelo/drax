import 'package:fish_redux/fish_redux.dart';
import 'package:fish_redux/fish_redux.dart' as fish;
import 'package:flutter/material.dart';

import 'state.dart';
import 'action.dart';
import '../components.dart';
import 'element_key.dart';



Widget buildView(RoomPageState state, Dispatch dispatch, ViewService viewService) {
  final ListAdapter adapter = viewService.buildAdapter();

  Function _if(ElementKey key, fish.Action action) { return !state.loading[key] ? () => dispatch(action) : null; }

  return Scaffold(
    body: 
      state.initialization 
        ? Center(child: CircularProgressIndicator(),)
        : Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              fit: BoxFit.cover,
              alignment: Alignment.center,
              colorFilter: new ColorFilter.mode(Color(0x30FFFFFF), BlendMode.dstATop),
              image: state.room.localImage 
                ? AssetImage(state.room.imageUrl)
                : NetworkImage(state.room.imageUrl)
            )
          ),
          child: LayoutBuilder(
            builder: (context, constraints) => SingleChildScrollView(
              child: Container(
                constraints: BoxConstraints(
                  minHeight: constraints.maxHeight,
                ),
                child: Column(
                  children: [
                    AppBar(
                      backgroundColor: Colors.transparent,
                      elevation: 0,
                      actions: [
                        IconButton(
                          icon: Icon(Icons.photo_library),
                          onPressed: _if(ElementKey.changePhoto, RoomActionCreate.showChangePhotoDialog()),
                        ),
                      ],
                    ),
                    Column(
                      children: [  
                        Container(
                          margin: EdgeInsets.only(top: 10),
                          child: Text(
                            state.room.name,
                            style: TextStyle(fontSize: 40, fontWeight: FontWeight.w700),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 20),
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: state.loading[ElementKey.switchFavorite] ? Colors.grey : Colors.white
                          ),
                          child: IconButton(
                            icon: 
                              state.room.favorite 
                                ? Icon(Icons.favorite, color: Color(0xFF00AEEF))
                                : Icon(Icons.favorite_border, color: Colors.black,),
                            onPressed:  _if(ElementKey.switchFavorite, RoomActionCreate.switchFavoriteState(!state.room.favorite)),
                          ),
                        ),
                        Container(
                          height: 95,
                          width: 95,
                          margin: EdgeInsets.only(top: 40),
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            border: Border.all(
                              color: state.loading[ElementKey.switchEnable] 
                                ? Colors.grey 
                                : state.room.enabled 
                                  ? Color(0xFF00AEEF) 
                                  : Colors.redAccent,
                              width: 3
                            )
                          ),
                          child: FlatButton(
                            shape: CircleBorder(),
                            child: Text(
                              state.devices.length.toString(), 
                              style: TextStyle(
                                fontSize: 40, 
                                color: state.loading[ElementKey.switchEnable] 
                                  ? Colors.grey 
                                  : state.room.enabled 
                                    ? Color(0xFF00AEEF) 
                                    : Colors.redAccent,
                              )
                            ),
                            onPressed: () {}// _if(ElementKey.switchEnable, RoomActionCreate.switchEnableState(!state.room.enabled)),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 8),
                          child: Text('Devices', style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500),)
                        ),
                        Padding(
                          padding: EdgeInsets.all(20),
                          child: LayoutBuilder(
                            builder: (context, constraints) {                          
                              final deviceWidth = 127.0;
                              final deviceCount = adapter.itemCount;
                              final horizontalCount = (constraints.constrainWidth() / deviceWidth).floor();
                              return ListView.builder(
                                shrinkWrap: true,
                                physics: NeverScrollableScrollPhysics(),
                                itemBuilder: (context, i) {
                                  if((i*horizontalCount) >= deviceCount) return null;
                                  final diff = deviceCount - (i*horizontalCount);
                                  return Center( 
                                    child: Row(
                                      mainAxisSize: MainAxisSize.min,
                                      children: List.generate(
                                        diff > horizontalCount ? horizontalCount : diff, 
                                        (j) { 
                                          return Builder(
                                            builder: (context) => adapter.itemBuilder(context, (i*horizontalCount)+j),
                                          );
                                        }
                                      )
                                    )
                                  );
                                },
                              );
                            },
                          )
                        )
                      ]
                    ),
                  ]
                )
              )
            )
          )
        )
  );
}