import 'package:flutter/material.dart';

enum ElementKey {
  changePhoto,
  switchEnable,
  switchFavorite
}