import 'package:fish_redux/fish_redux.dart';

import '../shared_states.dart';
import '../device/component.dart';
import 'state.dart';

class DeviceListAdapter extends DynamicFlowAdapter<RoomPageState> {
  DeviceListAdapter()
      : super(
          pool: <String, Component<Object>>{
            'device': DeviceComponent(),
          },
          connector: _DeviceListConnector(),
        );
}

class _DeviceListConnector extends ConnOp<RoomPageState, List<ItemBean>> {
  @override
  List<ItemBean> get(RoomPageState state) {
    if (state.devices?.isNotEmpty == true) {
      return state.devices
          .map<ItemBean>((data) => ItemBean('device', data))
          .toList(growable: true);
    } else {
      return <ItemBean>[];
    }
  }

  @override
  void set(RoomPageState state, List<ItemBean> devices) {
    if (devices?.isNotEmpty == true) {
      final data = devices.map((d) => d.data);
      state.devices = List.from(
          devices.map((bean) => bean.data).toList()
      );
    } else {
      state.devices = [];
    }
  }
}