import '../shared_states.dart';
import 'package:fish_redux/fish_redux.dart';
import 'element_key.dart';
import '../device/state.dart';

enum RoomAction {
  showChangePhotoDialog,
  changePhoto,
  uploadPhotoFromLocal,
  switchEnableState,
  switchFavoriteState,
  refresh,
  refreshSilent,
  setLoading,
  setInitialization,
}

class RoomActionCreate{
  static Action showChangePhotoDialog() => const Action(RoomAction.showChangePhotoDialog);
  static Action refresh({Function after}) => Action(RoomAction.refresh, payload: {'after': after});
  static Action refreshSilent(RoomState room, List<DeviceComponentState> devices) => 
    Action(
      RoomAction.refreshSilent, 
      payload: {
        'room' : room,
        'devices' : devices
      }
    );
  static Action switchEnableState(bool enabled) => Action(RoomAction.switchEnableState, payload: enabled);
  static Action switchFavoriteState(bool favorite) => Action(RoomAction.switchFavoriteState, payload: favorite);
  static Action setLoading(ElementKey key, bool value) => Action(RoomAction.setLoading, payload: {'key': key, 'value': value});
  static Action setInitialization(bool init) => Action(RoomAction.setInitialization, payload: init);
}