import 'package:fish_redux/fish_redux.dart';
import 'package:fish_redux/fish_redux.dart' as fish;
import 'package:flutter/material.dart';

import '../router.dart';
import 'state.dart';
import 'action.dart';

import '../shared_effects.dart';
import 'element_key.dart';
import '../device/state.dart';

Effect<RoomPageState> buildEffect(){
  return combineEffects({
    RoomAction.showChangePhotoDialog: _changePhoto,
    RoomAction.switchEnableState: _switchEnableState,
    RoomAction.switchFavoriteState: _switchFavoriteState,
    RoomAction.uploadPhotoFromLocal: (a, b) => {},
    RoomAction.refresh: _refresh,
    Lifecycle.initState: _initState
  });
}

Future<void> _refresh(fish.Action action, Context<RoomPageState> context) async {
  final state = context.state;
  if(state.room.id != null) {
    final after = action.payload['after'];
    final roomDoc = await state.database.rooms.document(state.room.id).get();
    final room = state.database.rooms.fromDocument(roomDoc);
    final devices = await state.database.devices.getByRoomDocument(roomDoc);
    context.dispatch(
      RoomActionCreate.refreshSilent(
        room, 
        devices.map((d) => DeviceComponentState()..device = d).toList()
      )
    );
    if(after != null) {
      after();
    }
  }
}

Future<void> _initState(fish.Action action, Context<RoomPageState> context) async {
  context.dispatch(RoomActionCreate.refresh(after: () => context.dispatch(RoomActionCreate.setInitialization(false))));
}

Future<void> _doAsync(Context<RoomPageState> context, ElementKey key, Future<void> action) async {
  context.dispatch(RoomActionCreate.setLoading(key, true));
  await action;
  context.dispatch(
    RoomActionCreate.refresh(
      after: () => context.dispatch(RoomActionCreate.setLoading(key, false))
    )
  );
}

Future<void> _switchEnableState(fish.Action action, Context<RoomPageState> context) async {
  final state = context.state;
  await _doAsync(context, ElementKey.switchEnable, () async {
    state.database.rooms.update(state.room.id, {'enabled' : action.payload});
  }());
}

Future<void> _switchFavoriteState(fish.Action action, Context<RoomPageState> context) async {
  final state = context.state;
  await _doAsync(context, ElementKey.switchFavorite, 
    state.database.rooms.update(state.room.id, {'favorite' : action.payload})
  );
}


Future<void> _changePhoto(fish.Action action, Context<RoomPageState> context) async {
  final db = context.state.database;
  final storage = context.state.storage;
  final room = context.state.room;

  final file = await showChangePhotoDialog(context.context);
  if(file == null) return;
  context.dispatch(RoomActionCreate.setLoading(ElementKey.changePhoto, true));
  final url = await storage.upload(file, 'room_${room.id}');
  await db.rooms.update(room.id, {
    'imageUrl': url,
    'localImage': false
  });
  context.dispatch(RoomActionCreate.refresh());
  context.dispatch(RoomActionCreate.setLoading(ElementKey.changePhoto, false));
}