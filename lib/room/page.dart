import 'package:fish_redux/fish_redux.dart';

import 'effect.dart';
import 'state.dart';
import 'view.dart';
import 'reducer.dart';
import 'device_adapter.dart';
import '../components.dart';

class RoomPage extends Page<RoomPageState, String> {
  RoomPage()
      : super(
          initState: initState,
          reducer: buildReducer(),
          effect: buildEffect(),
          view: buildView,
          dependencies: Dependencies(
            adapter: NoneConn() + DeviceListAdapter(),
          ),
          middleware: [
            logMiddleware(tag: 'RoomPage'),
          ],
        );
}