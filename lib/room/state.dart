import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/widgets.dart';

import '../mixins.dart';
import '../shared_states.dart';
import 'page.dart';
import 'element_key.dart';
import '../device/state.dart';

class RoomPageState with AuthState, DatabaseState, StorageState implements Cloneable<RoomPageState> {
  RoomState room;
  List<DeviceComponentState> devices;
  bool initialization;

  Map<ElementKey, bool> loading = Map.fromIterable(ElementKey.values, key: (v) => v, value: (v) => false);

  @override
  RoomPageState clone() {
    return RoomPageState()
      .. loading = loading
      .. devices = devices
      .. initialization = initialization
      .. room = room;
  }
}

RoomPageState initState(String id) {
  final RoomPageState state = RoomPageState()
    ..room = ( 
      RoomState()
        ..id = id
    )
    ..initialization = true;
  return state;
}