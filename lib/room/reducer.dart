import 'package:fish_redux/fish_redux.dart';

import 'action.dart';
import 'state.dart';

Reducer<RoomPageState> buildReducer() {
  return combineReducers(
    [
      asReducer({ RoomAction.refreshSilent : _refreshSilent, }),
      asReducer({ RoomAction.setLoading : _setLoading, }),
      asReducer({ RoomAction.setInitialization : _setInitialization, }),
    ]
  );
}

RoomPageState _refreshSilent(RoomPageState state, Action action) {
  return state.clone()
    ..room = action.payload['room']
    ..devices = action.payload['devices'];
}

RoomPageState _setLoading(RoomPageState state, Action action) {
  return state.clone()
    ..loading[action.payload['key']] = action.payload['value'];
}
RoomPageState _setInitialization(RoomPageState state, Action action) {
  return state.clone()
    ..initialization = action.payload;
}