import 'package:fish_redux/fish_redux.dart';
import 'package:fish_redux/fish_redux.dart' as fish;
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'state.dart';
import 'action.dart';
import '../router.dart';
import '../shared_states.dart';
import '../shared_effects.dart';


Effect<RegisterState> buildEffect(){
  return combineEffects({
      RegisterAction.register: _register,
      RegisterAction.login: _login,
      RegisterAction.createAccount: _createAccount,
      RegisterAction.goBack: (a , b) => goBack(b.context),
  });
}

Future<void> _createAccount(fish.Action action, Context<RegisterState> context) async {
  final id = action.payload['id'];
  final name = context.state.nameController.text;
  try {
    final user = UserState()
      ..id = id
      ..name = name;
    final exist = await context.state.database.users.exist(user.id);
    if(exist) throw Exception('User already exist');
    await context.state.database.users.createWithId(user, user.id);
    logInSilent(context.state.auth, Navigator.of(context.context));
    print('account created');
  }
  catch (ex) {
    showSnackMessage(context.state.scaffoldKey.currentState, ex.toString());
    print('account creation failed: ' + ex.toString());
  }
}

Future<void> _login(fish.Action action, Context<RegisterState> context) async {
  final email = context.state.emailEditController.text;
  final pass = context.state.passwordEditController.text;
  try {
    final authUser = await context.state.auth.signIn(email, pass);
    context.dispatch(RegisterActionCreate.createAccount(authUser.uid));
  }
  catch (ex) {
    print('login failed: ' + ex.toString());
  }
}

Future<void> _register(fish.Action action, Context<RegisterState> context) async {
  final email = context.state.emailEditController.text;
  final pass = context.state.passwordEditController.text;
  context.dispatch(RegisterActionCreate.setLoading(true));
  try {
    final authUser = await context.state.auth.signUp(email, pass);
    context.dispatch(RegisterActionCreate.createAccount(authUser.uid));
  }
  on PlatformException catch (ex) {
    if(ex.code == 'ERROR_EMAIL_ALREADY_IN_USE') {
      print('email in use, trying to login..');
      context.dispatch(RegisterActionCreate.login());
    }
  }
  catch (ex) {
    showSnackMessage(context.state.scaffoldKey.currentState, ex.toString());
    print('registration failed: ' + ex.toString());
  }
  finally {
    context.dispatch(RegisterActionCreate.setLoading(false));
  }
}