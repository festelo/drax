import 'package:fish_redux/fish_redux.dart';

enum RegisterAction{
  createAccount,
  register,
  login,
  goBack,
  changePhoto,
  setLoading
}

class RegisterActionCreate{
  static Action createAccount(String id) => Action(RegisterAction.createAccount, payload: {'id' : id});
  static Action register() => const Action(RegisterAction.register);
  static Action login() => const Action(RegisterAction.login);
  static Action goBack() => const Action(RegisterAction.goBack);
  static Action changePhoto() => const Action(RegisterAction.changePhoto);
  static Action setLoading(bool value) =>  Action(RegisterAction.setLoading, payload: value);
} 