import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

import '../components.dart';
import 'state.dart';
import 'action.dart';

Widget buildView(RegisterState state, Dispatch dispatch, ViewService viewService) {
  return Scaffold(
    key: state.scaffoldKey,
    body: Container(
      decoration: BoxDecoration(
        color: Color(0xFF373447)
      ),
      padding: const EdgeInsets.all(16.0),
      child: Column(
        children: [ 
          Expanded(
            child: Center(
              child: ConstrainedBox(
                constraints: BoxConstraints(
                  maxHeight: 475,
                  maxWidth: 375
                ),
                child: SingleChildScrollView(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Padding(
                        padding: EdgeInsets.only(bottom: 40),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: <Widget>[
                            Container(
                              width: 30,
                              child: IconButton(
                                padding: EdgeInsets.all(5),
                                alignment: Alignment.centerRight,
                                iconSize: 28,
                                icon: Icon(Icons.arrow_back),
                                onPressed: () => dispatch(RegisterActionCreate.goBack()),
                              ),
                            ),
                            Text(
                              'Register',
                              style: TextStyle(fontSize: 50, fontWeight: FontWeight.w700),
                            ),
                            Container(
                              width: 30,
                            ),
                          ],
                        ),
                      ),
                      ConstrainedBox(
                        constraints: BoxConstraints(
                          maxHeight: 320
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Column(
                              children: [
                                DraxTextField(
                                  leftIcon: Icon(Icons.tag_faces), 
                                  controller: state.nameController,
                                  hintText: 'First & Last name',
                                ),
                                DraxTextField(
                                  leftIcon: Icon(Icons.mail_outline), 
                                  controller: state.emailEditController,
                                  hintText: 'example@domain.com',
                                ),
                                DraxTextField(
                                  leftIcon: Icon(Icons.lock_outline), 
                                  controller: state.passwordEditController,
                                  obscureText: true,
                                  hintText: 'Password',
                                ),
                              ],
                            ),
                            Column(
                              children: [
                                state.loading 
                                  ? CircularProgressIndicator()
                                  : RectButton(
                                    width: 237, 
                                    text: 'Register',
                                    onPressed: () => dispatch(RegisterActionCreate.register()),
                                  ),
                                FlatButton(
                                  child: Text(
                                    'I accept "Terms and Conditions"', 
                                    style: TextStyle(fontSize: 12, color: Color(0xD5DBDBDB)),
                                  ), 
                                  onPressed: () {},
                                ),
                              ],
                            )
                          ],
                        ),
                      )
                    ]
                  )
                )
              )
            ),
          ),
        ]
      )
    )
  );
}