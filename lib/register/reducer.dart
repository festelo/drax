import 'package:fish_redux/fish_redux.dart';

import 'action.dart';
import 'state.dart';

Reducer<RegisterState> buildReducer() {
  return asReducer<RegisterState>(<Object, Reducer<RegisterState>>{
    RegisterAction.setLoading: _setLoading
  });
}

RegisterState _setLoading(RegisterState state, Action action) {
  return state.clone()
    ..loading = action.payload;
}