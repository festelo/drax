import 'package:fish_redux/fish_redux.dart';

import 'effect.dart';
import 'state.dart';
import 'view.dart';
import 'reducer.dart';

class RegisterPage extends Page<RegisterState, String> {
  RegisterPage()
      : super(
          initState: initState,
          reducer: buildReducer(),
          effect: buildEffect(),
          view: buildView,
          middleware: [
            logMiddleware(tag: 'RegisterPage'),
          ],
        );
}