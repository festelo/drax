import 'package:fish_redux/fish_redux.dart';
import 'package:fish_redux/fish_redux.dart' as fish;
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

import '../components.dart';
import 'state.dart';
import 'action.dart';
import 'element_key.dart';


Widget buildView(AdminUserPageState state, Dispatch dispatch, ViewService viewService) {
  Function _if(ElementKey key, fish.Action action) { return !state.buttonsLoading[key] ? () => dispatch(action) : null; }

  return Scaffold(
    key: state.scaffoldKey,
    body: Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          fit: BoxFit.cover,
          alignment: Alignment.topLeft,
          colorFilter: new ColorFilter.mode(Color(0xFF373447), BlendMode.dstATop),
          image: AssetImage('assets/backs/login.png')
        )
      ),
      padding: const EdgeInsets.symmetric(horizontal: 0.0),
      child: CustomScrollView( 
        slivers: [
          SliverAppBar(
            centerTitle: true,
            backgroundColor: Colors.transparent,
            title: Text('Admin panel', style: TextStyle(fontSize: 35, fontWeight: FontWeight.w500),),
          ),
          state.loading 
            ? SliverFillRemaining(child: Center(child: CircularProgressIndicator()))
            : SliverPadding(
              padding: EdgeInsets.symmetric(horizontal: 16),
              sliver: SliverList(
                delegate: SliverChildListDelegate(
                  [
                    Padding(
                      padding: EdgeInsets.only(top: 20),
                      child: Text('User info', style: TextStyle(fontSize: 30, fontWeight: FontWeight.w500), textAlign: TextAlign.center,),
                    ),
                    FlatButton(
                      child: Text(state.user.id ?? 'Not saved user'),
                      onPressed: state.user.id == null 
                        ? null 
                        : () => dispatch(AdminUserActionCreate.saveId()),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 20),
                      padding: EdgeInsets.only(top: 0),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                        border: Border.all(
                          color: Colors.grey,
                          width: 2
                        )
                      ),
                      child: Column(
                        children: [
                          DraxTextField(
                            leftIcon: Icon(Icons.tag_faces), 
                            controller: state.nameController,
                            hintText: 'First & Last name',
                            padding: EdgeInsets.only(top: 10),
                            scrollPadding: EdgeInsets.all(80),
                          ),
                          RectButton(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.vertical(bottom: Radius.circular(10)),
                              border: Border.all(width: 1)
                            ),
                            margin: EdgeInsets.only(top: 1),
                            text: 'Save',
                            onPressed: _if(ElementKey.userInfoSave, AdminUserActionCreate.changeUserInfo()),
                          )
                        ],
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 35),
                      child: Text('Email', style: TextStyle(fontSize: 30, fontWeight: FontWeight.w500), textAlign: TextAlign.center),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 20),
                      padding: EdgeInsets.only(top: 0),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                        border: Border.all(
                          color: Colors.grey,
                          width: 2
                        )
                      ),
                      child: Column(
                        children: [
                          DraxTextField(
                            leftIcon: Icon(Icons.mail_outline), 
                            controller: state.emailEditController,
                            hintText: 'example@domain.com',
                            padding: EdgeInsets.only(top: 10),
                            scrollPadding: EdgeInsets.all(80),
                          ),
                          RectButton(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.vertical(bottom: Radius.circular(10)),
                              border: Border.all(width: 1)
                            ),
                            margin: EdgeInsets.only(top: 1),
                            text: 'Save',
                            onPressed: _if(ElementKey.emailSave, AdminUserActionCreate.changeEmail()),
                          )
                        ],
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 35),
                      child: Text('Password', style: TextStyle(fontSize: 30, fontWeight: FontWeight.w500), textAlign: TextAlign.center),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 20),
                      padding: EdgeInsets.only(top: 0),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                        border: Border.all(
                          color: Colors.grey,
                          width: 2
                        )
                      ),
                      child: Column(
                        children: [
                          DraxTextField(
                            leftIcon: Icon(Icons.lock_outline), 
                            controller: state.passwordEditController,
                            obscureText: true,
                            hintText: 'Password',
                            padding: EdgeInsets.only(top: 10),
                            scrollPadding: EdgeInsets.all(80),
                          ),
                          RectButton(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.vertical(bottom: Radius.circular(10)),
                              border: Border.all(width: 1)
                            ),
                            margin: EdgeInsets.only(top: 1),
                            text: 'Save',
                            onPressed: _if(ElementKey.passwordSave, AdminUserActionCreate.changePassword()),
                          )
                        ],
                      ),
                    ),
                    RectButton(
                      decoration: BoxDecoration(
                        border: Border.all(color: Colors.grey, width: 2)
                      ),
                      margin: EdgeInsets.only(top: 40),
                      text: 'Change photo',
                      onPressed: _if(ElementKey.changePhoto, AdminUserActionCreate.changePhoto()),
                    )
                  ]
                ),
              ) 
            )
          ]
        )
    )
  );
}