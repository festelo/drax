import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'page.dart';
import '../shared_states.dart';
import '../mixins.dart';
import 'element_key.dart';

class AdminUserPageState with DatabaseState, StorageState, FunctionsState implements Cloneable<AdminUserPageState> {
  GlobalKey<State<Scaffold>> scaffoldKey;
  TextEditingController nameController;
  TextEditingController emailEditController;
  TextEditingController passwordEditController;
  UserState user;
  bool loading;
  Map<ElementKey, bool> buttonsLoading = Map.fromIterable(ElementKey.values, key: (v) => v, value: (v) => false);

  @override
  AdminUserPageState clone() {
    return AdminUserPageState()
      ..loading = loading
      ..buttonsLoading = buttonsLoading
      ..scaffoldKey = scaffoldKey
      ..nameController = nameController
      ..emailEditController = emailEditController
      ..passwordEditController = passwordEditController
      ..user = user;
  }
}

AdminUserPageState initState(String userId) {
  final state = AdminUserPageState();
  state.scaffoldKey = GlobalKey();
  state.nameController = TextEditingController();
  state.emailEditController = TextEditingController();
  state.passwordEditController = TextEditingController();
  state.loading = false;
  state.user = UserState()
    ..id = userId;
  return state;
}