import 'package:fish_redux/fish_redux.dart';
import 'package:fish_redux/fish_redux.dart' as fish;
import 'package:flutter/material.dart';

import '../shared_effects.dart';
import '../router.dart';
import 'state.dart';
import 'action.dart';
import 'element_key.dart';

Effect<AdminUserPageState> buildEffect(){
  return combineEffects({
    AdminUserAction.saveId: (a, c) => saveToClipboard(c.state.user.id),
    AdminUserAction.changePassword: _changePassword,
    AdminUserAction.changeUserInfo: _changeUserInfo,
    AdminUserAction.changePhoto: _changePhoto,
    AdminUserAction.changeEmail: _changeEmail,
    AdminUserAction.goBack: _goBack,
    Lifecycle.initState: _initState
  });
}

Future<void> _initState(fish.Action action, Context<AdminUserPageState> context) async {
  if(context.state.user.id != null) {
    context.dispatch(AdminUserActionCreate.setLoading(true));
    final user = await context.state.database.users.get(context.state.user.id);
    context.dispatch(AdminUserActionCreate.refreshSilent(user));
    context.dispatch(AdminUserActionCreate.setLoading(false));
  }
}

void _goBack(fish.Action action, Context<AdminUserPageState> context){
  goBack(context.context);
}

Future<void> _changePhoto(fish.Action action, Context<AdminUserPageState> context) async {
  final db = context.state.database;
  final storage = context.state.storage;
  final user = context.state.user;

  if(user.id == null) {
    showSnackMessage(context.state.scaffoldKey.currentState, 'You should to set info first');
    return;
  }
  else {
    final file = await showChangePhotoDialog(context.context);
    if(file == null) return;
    context.dispatch(AdminUserActionCreate.setLoading(true, element: ElementKey.changePhoto));
    try{
      final url = await storage.upload(file, 'user_${user.id}');
      await db.users.update(user.id, {
        'photoUrl': url,
        'localImage': false
      });
    }
    catch (e) {
      showSnackMessage(context.state.scaffoldKey.currentState, e.toString());
    }
    finally {
      context.dispatch(AdminUserActionCreate.setLoading(false, element: ElementKey.changePhoto));
    }
  }
}

Future<void> _changeUserInfo(fish.Action action, Context<AdminUserPageState> context) async {
  context.dispatch(AdminUserActionCreate.setLoading(true, element: ElementKey.userInfoSave));
  await context.state.database.users.update(
    context.state.user.id, 
    {
      'name' : context.state.nameController.text
    }
  );
  context.dispatch(AdminUserActionCreate.setLoading(false, element: ElementKey.userInfoSave));
}

Future<void> _changeEmail(fish.Action action, Context<AdminUserPageState> context) async {
  context.dispatch(AdminUserActionCreate.setLoading(true, element: ElementKey.emailSave));
  try {
    final res = await context.state.functions.call('changeEmail', {
      'userId': context.state.user.id,
      'email': context.state.emailEditController.text
    });
  }
  catch (e) {
    showSnackMessage(context.state.scaffoldKey.currentState, e.toString());
  }
  finally {
    context.dispatch(AdminUserActionCreate.setLoading(false, element: ElementKey.emailSave));
  }
}

Future<void> _changePassword(fish.Action action, Context<AdminUserPageState> context) async {
  context.dispatch(AdminUserActionCreate.setLoading(true, element: ElementKey.passwordSave));
  try {
    final res = await context.state.functions.call('changePassword', {
      'userId': context.state.user.id,
      'password': context.state.passwordEditController.text
    });
  }
  catch (e) {
    showSnackMessage(context.state.scaffoldKey.currentState, e.toString());
  }
  finally {
    context.dispatch(AdminUserActionCreate.setLoading(false, element: ElementKey.passwordSave));
  }
}