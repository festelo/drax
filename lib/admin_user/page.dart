import 'package:fish_redux/fish_redux.dart';

import 'effect.dart';
import 'state.dart';
import 'view.dart';
import 'reducer.dart';

class AdminUserPage extends Page<AdminUserPageState, String> {
  AdminUserPage()
      : super(
          initState: initState,
          effect: buildEffect(),
          reducer: buildReducer(),
          view: buildView,
          middleware: [
            logMiddleware(tag: 'AdminUserPage'),
          ],
        );
}