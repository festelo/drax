import 'package:flutter/material.dart';

enum ElementKey {
  userInfoSave,
  emailSave,
  passwordSave,
  changePhoto
}