import 'package:fish_redux/fish_redux.dart';
import '../shared_states.dart';
import 'element_key.dart';

enum AdminUserAction{
  setLoading,
  refreshSilent,
  changeEmail,
  changePassword,
  changeUserInfo,
  changePhoto,
  saveId,
  goBack
}

class AdminUserActionCreate{
  static Action refreshSilent(UserState user) => Action(AdminUserAction.refreshSilent, payload: { 'user' : user});
  static Action setLoading(bool loading, {ElementKey element}) => Action(
    AdminUserAction.setLoading, 
    payload: {
      'value': loading,
      'element': element
    }
  );
  static Action changeEmail() => const Action(AdminUserAction.changeEmail);
  static Action changePassword() => const Action(AdminUserAction.changePassword);
  static Action changeUserInfo() => const Action(AdminUserAction.changeUserInfo);
  static Action changePhoto() => const Action(AdminUserAction.changePhoto);
  static Action saveId() => const Action(AdminUserAction.saveId);
  static Action goBack() => const Action(AdminUserAction.goBack);
}