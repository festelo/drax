
import 'package:flutter/material.dart';

class RoomCard extends StatelessWidget {
  RoomCard({this.height = 180, this.text, this.paddingUnderTitle = 20, @required this.background});
  final double height;
  final double paddingUnderTitle;
  final String text;
  final ImageProvider<dynamic> background;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          fit: BoxFit.fitWidth,
          image: background
        )
      ),
      height: height,
      width: double.infinity,
      child: Container(
        alignment: Alignment.bottomRight,
        padding: EdgeInsets.only(bottom: paddingUnderTitle),
        child: Container(
          decoration: BoxDecoration(
            color: Color(0xC4000000)
          ),
          width: 120,
          padding: EdgeInsets.all(4),
          child: Text(
            text,
            maxLines: 3,
            style: TextStyle(fontSize: 15, fontWeight: FontWeight.w500), 
            textAlign: TextAlign.center,
          )
        )
      )
    );
  }
}