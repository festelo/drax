
import 'package:flutter/material.dart';

class RectButton extends StatelessWidget {
  RectButton({this.width = double.infinity, this.margin, this.decoration, this.height = 44, this.text, @required this.onPressed});
  final double width;
  final double height;
  final String text;
  final VoidCallback onPressed;
  final Decoration decoration;
  final EdgeInsetsGeometry margin;
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: margin,
      height: height,
      width: width,
      decoration: decoration,
      child: RaisedButton(
        color: Color(0xFF211F2A),
        onPressed: onPressed,
        child: Text(text),
      )
    );
  }
}