
import 'package:flutter/material.dart';

class BoxButton extends StatelessWidget {
  BoxButton({
    this.width = 107, 
    this.height = 107, 
    this.padding, 
    this.margin = const EdgeInsets.all(10), 
    this.border = const RoundedRectangleBorder(
      borderRadius:  const BorderRadius.all(Radius.circular(10)),
      side: const BorderSide(width: 0)
    ),
    this.child, 
    this.decoration, 
    this.color = const Color(0xE5FFFFFF),
    this.splashColor = const Color(0xFF90DBD8),
    @required this.onPressed
  });
  final double width;
  final double height;
  final Decoration decoration;
  final Widget child;
  final VoidCallback onPressed;
  final EdgeInsets padding; 
  final EdgeInsets margin; 
  final ShapeBorder border;
  final Color color;
  final Color splashColor;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: decoration,
      height: height,
      width: width,
      padding: padding,
      margin: margin,
      child: FlatButton(
        padding: EdgeInsets.all(0),
        shape: border,
        color: color,
        splashColor: splashColor,
        onPressed: onPressed,
        child: Center(
          child: child
        ),
      )
    );
  }
}