
import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';

class DraxTextField extends StatefulWidget {
  DraxTextField({this.leftIcon, this.scrollPadding = const EdgeInsets.all(20), this.padding = const EdgeInsets.symmetric(vertical: 6), this.hintText, this.obscureText = false, @required this.controller});
  final Widget leftIcon;
  final TextEditingController controller;
  final String hintText;
  final EdgeInsetsGeometry padding;
  final EdgeInsetsGeometry scrollPadding;
  final bool obscureText;

  @override
  _DraxTextField createState() => _DraxTextField();
}

class _DraxTextField extends State<DraxTextField> {
  final FocusNode focusNode = FocusNode();
  bool showClearButton = false;

  _DraxTextField(){
    focusNode.addListener(() => !mounted 
      ? null 
      : setState(() => showClearButton =focusNode.hasFocus)
    );
  }

  @override
  void dispose() {
    super.dispose();
    focusNode.dispose();
  }
  
  Widget _wrapIntoContainer(Widget child){
    return Container(
      width: 10,
      child: child,
    );
  }

  @override
  Widget build(BuildContext context) => Padding(
    padding: widget.padding,
    child: TextField(
      scrollPadding: widget.scrollPadding,
      textAlign: TextAlign.center,
      controller: widget.controller,
      focusNode: focusNode,
      obscureText: widget.obscureText,
      decoration: InputDecoration(
        hintText: widget.hintText,
        prefixIcon: _wrapIntoContainer(widget.leftIcon),
        suffixIcon: _wrapIntoContainer( 
          !showClearButton 
            ? Container()
            : IconButton(
                icon: Icon(Icons.clear),
                onPressed: widget.controller.clear,
              )
        )
      ),
    ),
  );
}