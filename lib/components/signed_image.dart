
import 'package:flutter/material.dart';

class SignedImage extends StatelessWidget {
  SignedImage({@required this.image, this.text = ''});
  final ImageProvider<dynamic> image;
  final String text;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [ 
        Expanded(
          child: AspectRatio(
            aspectRatio: 1,
            child: Container(
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                border: Border.all(
                  color: Colors.grey,
                  width: 2
                ),
                image: DecorationImage(
                  fit: BoxFit.cover,
                  image: image
                )
              ),
            ),
          )
        ),
        Text(text, maxLines: 1, style: TextStyle(fontSize: 13, fontWeight: FontWeight.w500), textAlign: TextAlign.center,)
      ]
    );
  }
}