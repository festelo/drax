import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';

import '../action.dart';
import '../component.dart';
import '../../components.dart';

Widget imageFilledDevice(Dispatch dispatch, DeviceComponentState state) {
  return BoxButton(
    margin: EdgeInsets.all(5),
    decoration: BoxDecoration(
      borderRadius: BorderRadius.all(Radius.circular(10)),
      image: DecorationImage(
        fit: BoxFit.cover,
        alignment: Alignment.topLeft,
        image: state.device.localImage 
          ? AssetImage(state.device.imageUrl)
          : NetworkImage(state.device.imageUrl)
      )
    ),
    border: RoundedRectangleBorder(
      borderRadius: BorderRadius.all(Radius.circular(10)),
      side: !state.device.enabled 
        ? BorderSide.none
        : BorderSide(
          color: Color(0xFF00AEEF),
          width: 3
        )
    ),
    child: Container(
      width: double.infinity,
      height: double.infinity,
      child: Align(
        alignment: Alignment.bottomCenter,
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.vertical(bottom: Radius.circular(10)),
            color: Color(0xAF000000),
          ),
          width: double.infinity,
          padding: EdgeInsets.only(top: 4, bottom: 4),
          child: Text(state.device.name, textAlign: TextAlign.center),
        )
      )
    ),
    color: Colors.transparent,
    onPressed: () => dispatch(DeviceActionCreate.switchEnableState(!state.device.enabled)),
  );
}