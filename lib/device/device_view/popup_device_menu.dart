import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';

import '../action.dart';

class PopupDeviceMenu extends StatefulWidget {
  PopupDeviceMenu({
    @required this.dispatch,
    @required this.actions,
    @required this.child,
    this.loading = false,
  });

  final Dispatch dispatch;
  final Widget child;
  final Map<String, String> actions;
  final bool loading;

  @override
  State<StatefulWidget> createState() => _PopupDeviceMenu();
}

class _PopupDeviceMenu extends State<PopupDeviceMenu> {
  Offset tapPosition;

  @override
  Widget build(BuildContext context) {
    final RenderBox overlay = Overlay.of(context).context.findRenderObject();
    return GestureDetector(
      onTapDown: (details) => 
        tapPosition = details.globalPosition,
      onLongPress: widget.loading 
        ? () {} 
        : () async {
          final name = await showMenu(
            position: RelativeRect.fromRect(
                tapPosition & Size(40, 40), 
                Offset.zero & overlay.size 
            ),
            items: widget.actions?.keys?.map((name) => 
              PopupMenuItem(
                value: name,
                child: Center(child: Text(name)),
              )
            )?.toList() ?? [],
            context: context,
          );
          if(name == null) return;
          widget.dispatch(DeviceActionCreate.makeAction(name));
        },
      child: widget.child,
    );
  } 
}