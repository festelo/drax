import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';

import '../action.dart';
import '../component.dart';
import '../../components.dart';

Widget imageCenteredDevice(Dispatch dispatch, DeviceComponentState state) {
  return BoxButton(
    onPressed: () => dispatch(DeviceActionCreate.switchEnableState(!state.device.enabled)),
    border: RoundedRectangleBorder(
      borderRadius: BorderRadius.all(Radius.circular(10)),
      side:  BorderSide(
        color: Color(0xFF00AEEF),
        width: state.device.enabled ? 3 : 0
      )
    ),
    margin: EdgeInsets.all(5),
    child:
      state.loading 
        ? CircularProgressIndicator()
        : Column( 
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Padding(
                padding: EdgeInsets.all(8),
                child: state.device.localImage 
                  ? Image.asset(state.device.imageUrl, width: 45, height: 45,)
                  : Image.network(state.device.imageUrl, width: 45, height: 45,),
              ),
              Text(state.device.name, style: TextStyle(color: Colors.black, fontSize: 14, fontWeight: FontWeight.w600), textAlign: TextAlign.center, maxLines: 1,)
            ]
          )
  );
}