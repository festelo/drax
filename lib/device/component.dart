import 'package:fish_redux/fish_redux.dart';

import 'view.dart';
import 'effect.dart';
import 'reducer.dart';
import '../shared_states.dart';
import 'state.dart';
export 'state.dart';

class DeviceComponent extends Component<DeviceComponentState> with PrivateReducerMixin<DeviceComponentState> {
  DeviceComponent()
      : super(
          reducer: buildReducer(),
          view: buildView,
          effect: buildEffect(),
        );
}