
import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';

import 'device_view/view.dart';

import '../shared_states.dart';
import '../components.dart';
import 'action.dart';
import 'state.dart';


Widget buildView(DeviceComponentState state, Dispatch dispatch, ViewService viewService) { 
  return PopupDeviceMenu(
    dispatch: dispatch, 
    actions: state.device.customActions,
    loading: state.loading, 
    child: state.device.filledImage 
      ? imageFilledDevice(dispatch, state)
      : imageCenteredDevice(dispatch, state)
  );
}