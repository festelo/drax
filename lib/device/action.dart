
import 'package:fish_redux/fish_redux.dart';
import '../shared_states.dart';
import 'state.dart';

enum DeviceAction{
  switchEnableState,
  switchEnableStateSilent,
  makeAction,
  setLoading,
  refresh,
  refreshSilent
}

class DeviceActionCreate{
  static Action makeAction(String name) => Action(DeviceAction.makeAction, payload: name );
  static Action switchEnableState(bool enable) => Action(DeviceAction.switchEnableState, payload: enable );
  static Action setLoading(bool value) => Action(DeviceAction.setLoading, payload:  value );
  static Action refresh() => Action(DeviceAction.refresh );
  static Action refreshSilent(DeviceState device) => Action(DeviceAction.refreshSilent, payload: { 'device': device } );
}