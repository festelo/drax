
import 'package:fish_redux/fish_redux.dart';
import 'package:flutter/widgets.dart';

import '../mixins.dart';
import '../shared_states.dart';


class DeviceComponentState with DatabaseState implements Cloneable<DeviceComponentState> {
  DeviceState device;
  bool loading = false;

  @override
  DeviceComponentState clone() {
    return DeviceComponentState()
      .. loading = loading
      .. device = device;
  }
}