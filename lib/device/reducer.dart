import 'package:fish_redux/fish_redux.dart';

import 'action.dart';
import '../shared_states.dart';
import 'state.dart';

Reducer<DeviceComponentState> buildReducer() {
  return asReducer(
    { 
      DeviceAction.setLoading : _setLoading, 
      DeviceAction.refreshSilent : _refreshSilent, 
    }
  );
}

DeviceComponentState _setLoading(DeviceComponentState state, Action action) {
  return state.clone()
    ..loading = action.payload;
}
DeviceComponentState _refreshSilent(DeviceComponentState state, Action action) {
  return state.clone()
    ..device = action.payload['device'];
}