import 'package:fish_redux/fish_redux.dart';

import '../shared_states.dart';
import 'action.dart';
import 'state.dart';
import 'package:http/http.dart' as http;

Effect<DeviceComponentState> buildEffect(){
  return combineEffects({
      DeviceAction.switchEnableState: _switchEnableState,
      DeviceAction.makeAction: _makeAction,
      DeviceAction.refresh: _refresh,
  });
}

Future<void> _makeAction(Action action, Context<DeviceComponentState> context) async {
  final state = context.state;
  final url = state.device.customActions[action.payload];
  
  context.dispatch(DeviceActionCreate.setLoading(true));
  var response = await http.get(url);
  context.dispatch(DeviceActionCreate.setLoading(false));
}

Future<void> _switchEnableState(Action action, Context<DeviceComponentState> context) async {
  final state = context.state;
  final device = state.device;
  final bool value = action.payload;

  context.dispatch(DeviceActionCreate.setLoading(true));
  if(value) {
    var response = await http.get(state.device.onUrl);
  } else {
    var response = await http.get(state.device.offUrl);
  }
  await state.database.devices.update(state.device.id, {'enabled': action.payload});
  context.dispatch(DeviceActionCreate.refresh());
}

Future<void> _refresh(Action action, Context<DeviceComponentState> context) async {
  final state = context.state;

  context.dispatch(DeviceActionCreate.setLoading(true));
  final device = await state.database.devices.get(state.device.id);
  context.dispatch(DeviceActionCreate.refreshSilent(device));
  context.dispatch(DeviceActionCreate.setLoading(false));
}